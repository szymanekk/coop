// ignore_for_file: file_names

import 'package:coop/repositories/RouteRepository.dart';
import 'package:coop/repositories/RouteRepositoryBase.dart';
import 'package:coop/repositories/ShopRepository.dart';
import 'package:coop/repositories/ShopRepositoryBase.dart';
import 'package:coop/services/RouteService.dart';
import 'package:coop/services/clients/ClientBase.dart';
import 'package:coop/services/clients/DioClient.dart';
import 'package:coop/services/ShopService.dart';
import 'package:coop/services/ShopServiceBase.dart';
import 'package:coop/services/routeServiceBase.dart';
import 'package:get_it/get_it.dart';

final getIt = GetIt.instance;

class Di {
  static void init() {
    registerServices();
    registerRepositories();
  }

  static void registerServices() {
    getIt.registerLazySingleton<ClientBase>(
          () => DioClient(),
    );
    getIt.registerLazySingleton<ShopServiceBase>(
      () => ShopService(getIt()),
    );
    getIt.registerLazySingleton<RouteServiceBase>(
          () => RouteService(getIt()),
    );
  }

  static void registerRepositories() {
    getIt.registerLazySingleton<ShopRepositoryBase>(
          () => ShopRepository(getIt()));
    getIt.registerLazySingleton<RouteRepositoryBase>(
            () => RouteRepository(getIt()));
  }
}
