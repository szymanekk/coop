// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "closed": MessageLookupByLibrary.simpleMessage("Closed"),
        "closedNow":
            MessageLookupByLibrary.simpleMessage("Shop is currently closed."),
        "connectionIssue":
            MessageLookupByLibrary.simpleMessage("Connection issue!"),
        "coop": MessageLookupByLibrary.simpleMessage("Coop"),
        "downloadNewsletter":
            MessageLookupByLibrary.simpleMessage("Download newsletter"),
        "errorOccurred": MessageLookupByLibrary.simpleMessage("Error"),
        "goBack": MessageLookupByLibrary.simpleMessage("Go back"),
        "navigate": MessageLookupByLibrary.simpleMessage("Navigate"),
        "newsletterIssue":
            MessageLookupByLibrary.simpleMessage("Newsletter issue!"),
        "noShops": MessageLookupByLibrary.simpleMessage("No shops"),
        "noShopsDescription": MessageLookupByLibrary.simpleMessage(
            "There are no shops available"),
        "ok": MessageLookupByLibrary.simpleMessage("ok"),
        "open": MessageLookupByLibrary.simpleMessage("Open"),
        "openDetails": MessageLookupByLibrary.simpleMessage("Open details"),
        "openNow": MessageLookupByLibrary.simpleMessage("Shop is open!"),
        "repeat": MessageLookupByLibrary.simpleMessage("Repeat"),
        "searchByPost":
            MessageLookupByLibrary.simpleMessage("search by post number"),
        "shopsNearYou": MessageLookupByLibrary.simpleMessage("Shops near you"),
        "stopNavigation":
            MessageLookupByLibrary.simpleMessage("Stop navigation"),
        "unauthenticatedIssue":
            MessageLookupByLibrary.simpleMessage("Unauthenticated!"),
        "unknown":
            MessageLookupByLibrary.simpleMessage("Unexpected error occurred.")
      };
}
