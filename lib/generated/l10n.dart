// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars
// ignore_for_file: join_return_with_assignment, prefer_final_in_for_each
// ignore_for_file: avoid_redundant_argument_values, avoid_escaping_inner_quotes

class S {
  S();

  static S? _current;

  static S get current {
    assert(_current != null,
        'No instance of S was loaded. Try to initialize the S delegate before accessing S.current.');
    return _current!;
  }

  static const AppLocalizationDelegate delegate = AppLocalizationDelegate();

  static Future<S> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false)
        ? locale.languageCode
        : locale.toString();
    final localeName = Intl.canonicalizedLocale(name);
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      final instance = S();
      S._current = instance;

      return instance;
    });
  }

  static S of(BuildContext context) {
    final instance = S.maybeOf(context);
    assert(instance != null,
        'No instance of S present in the widget tree. Did you add S.delegate in localizationsDelegates?');
    return instance!;
  }

  static S? maybeOf(BuildContext context) {
    return Localizations.of<S>(context, S);
  }

  /// `Shops near you`
  String get shopsNearYou {
    return Intl.message(
      'Shops near you',
      name: 'shopsNearYou',
      desc: '',
      args: [],
    );
  }

  /// `search by post number`
  String get searchByPost {
    return Intl.message(
      'search by post number',
      name: 'searchByPost',
      desc: '',
      args: [],
    );
  }

  /// `Shop is open!`
  String get openNow {
    return Intl.message(
      'Shop is open!',
      name: 'openNow',
      desc: '',
      args: [],
    );
  }

  /// `Shop is currently closed.`
  String get closedNow {
    return Intl.message(
      'Shop is currently closed.',
      name: 'closedNow',
      desc: '',
      args: [],
    );
  }

  /// `Open details`
  String get openDetails {
    return Intl.message(
      'Open details',
      name: 'openDetails',
      desc: '',
      args: [],
    );
  }

  /// `Open`
  String get open {
    return Intl.message(
      'Open',
      name: 'open',
      desc: '',
      args: [],
    );
  }

  /// `Closed`
  String get closed {
    return Intl.message(
      'Closed',
      name: 'closed',
      desc: '',
      args: [],
    );
  }

  /// `Navigate`
  String get navigate {
    return Intl.message(
      'Navigate',
      name: 'navigate',
      desc: '',
      args: [],
    );
  }

  /// `Stop navigation`
  String get stopNavigation {
    return Intl.message(
      'Stop navigation',
      name: 'stopNavigation',
      desc: '',
      args: [],
    );
  }

  /// `Download newsletter`
  String get downloadNewsletter {
    return Intl.message(
      'Download newsletter',
      name: 'downloadNewsletter',
      desc: '',
      args: [],
    );
  }

  /// `ok`
  String get ok {
    return Intl.message(
      'ok',
      name: 'ok',
      desc: '',
      args: [],
    );
  }

  /// `No shops`
  String get noShops {
    return Intl.message(
      'No shops',
      name: 'noShops',
      desc: '',
      args: [],
    );
  }

  /// `There are no shops available`
  String get noShopsDescription {
    return Intl.message(
      'There are no shops available',
      name: 'noShopsDescription',
      desc: '',
      args: [],
    );
  }

  /// `Error`
  String get errorOccurred {
    return Intl.message(
      'Error',
      name: 'errorOccurred',
      desc: '',
      args: [],
    );
  }

  /// `Unexpected error occurred.`
  String get unknown {
    return Intl.message(
      'Unexpected error occurred.',
      name: 'unknown',
      desc: '',
      args: [],
    );
  }

  /// `Coop`
  String get coop {
    return Intl.message(
      'Coop',
      name: 'coop',
      desc: '',
      args: [],
    );
  }

  /// `Unauthenticated!`
  String get unauthenticatedIssue {
    return Intl.message(
      'Unauthenticated!',
      name: 'unauthenticatedIssue',
      desc: '',
      args: [],
    );
  }

  /// `Newsletter issue!`
  String get newsletterIssue {
    return Intl.message(
      'Newsletter issue!',
      name: 'newsletterIssue',
      desc: '',
      args: [],
    );
  }

  /// `Connection issue!`
  String get connectionIssue {
    return Intl.message(
      'Connection issue!',
      name: 'connectionIssue',
      desc: '',
      args: [],
    );
  }

  /// `Go back`
  String get goBack {
    return Intl.message(
      'Go back',
      name: 'goBack',
      desc: '',
      args: [],
    );
  }

  /// `Repeat`
  String get repeat {
    return Intl.message(
      'Repeat',
      name: 'repeat',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<S> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'en'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<S> load(Locale locale) => S.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    for (var supportedLocale in supportedLocales) {
      if (supportedLocale.languageCode == locale.languageCode) {
        return true;
      }
    }
    return false;
  }
}
