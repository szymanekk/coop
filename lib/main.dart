import 'package:coop/modules/SplashPage.dart';
import 'package:coop/resources/AppRoutes.dart';
import 'package:coop/resources/AppThemes.dart';
import 'package:coop/services/clients/ClientBase.dart';
import 'package:flutter/material.dart';

import 'Di.dart';
import 'generated/l10n.dart';

final RouteObserver<ModalRoute<dynamic>> routeObserver =
RouteObserver<ModalRoute<dynamic>>();

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  Di.init();
  getIt<ClientBase>().init();

  runApp(const App(),);
}

class App extends StatefulWidget {
  const App({super.key});

  @override
  AppState createState() => AppState();
}

class AppState extends State<App> with WidgetsBindingObserver, RouteAware {
  static final GlobalKey<NavigatorState> navigatorKey =
  GlobalKey(debugLabel: 'navigator');

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) => MaterialApp(
    navigatorKey: navigatorKey,
    navigatorObservers: [routeObserver],
    initialRoute: SplashPage.routeName,
    localizationsDelegates: const [
      S.delegate,
    ],
    supportedLocales: S.delegate.supportedLocales,
    onGenerateRoute: (RouteSettings settings) =>
        MaterialPageRoute<dynamic>(
          settings: settings,
          builder: (context) =>
              AppRoutes.routeBuilders[settings.name]!(context),
        ),
    // routes: Routes.routeBuilders,
    debugShowCheckedModeBanner: false,
    theme: AppThemes.appTheme(),
    themeMode: ThemeMode.dark,
  );
}
