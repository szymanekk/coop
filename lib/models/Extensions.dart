import 'package:geolocator/geolocator.dart';
import 'package:latlong2/latlong.dart';

extension PositionExt on Position {
  LatLng get latlng => LatLng(latitude, longitude);
}