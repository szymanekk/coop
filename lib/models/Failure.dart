import 'dart:async';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:coop/generated/l10n.dart';
import 'package:coop/resources/AppConstants.dart';

part 'Failure.freezed.dart';

@freezed
class Failure with _$Failure {
  const Failure._();

  const factory Failure.unauthenticated({String? message}) =
      UnauthenticatedError;

  const factory Failure.unknown({String? message}) = UnknownError;

  const factory Failure.connectionError({String? message}) = ConnectionError;

  const factory Failure.newsletterError({String? message}) = NewsletterError;

  factory Failure.fromDioError({required DioException error}) {
    switch (error.type) {
      case DioExceptionType.connectionTimeout:
      case DioExceptionType.sendTimeout:
      case DioExceptionType.receiveTimeout:
      case DioExceptionType.badCertificate:
      case DioExceptionType.connectionError:
        return Failure.connectionError(message: error.message);
      case DioExceptionType.badResponse:
        if (error.response?.statusCode == ConstantResponseCodes.forbidden) {
          return Failure.unauthenticated(message: error.message);
        }
      default:
        break;
    }
    return const Failure.unknown();
  }

  factory Failure.fromException({required Object error}) {
    if(error is DioException){
      return Failure.fromDioError(error: error);
    }

    if(error is TimeoutException){
      return const Failure.connectionError();
    }

    return const Failure.unknown();
  }

  String getReadable(BuildContext context) =>
      maybeMap(
          unauthenticated: (unauthenticated) => S.of(context).unauthenticatedIssue,
          newsletterError: (newsletter) => S.of(context).newsletterIssue,
          connectionError: (connection) => S.of(context).connectionIssue,
          orElse: () => S.of(context).unknown);

  IconData getIcon(BuildContext context) =>
      maybeMap(
          unauthenticated: (unauthenticated) => Icons.warning,
          newsletterError: (newsletter) => Icons.disabled_by_default_rounded,
          connectionError: (connection) => Icons.network_locked_outlined,
          orElse: () => Icons.thumb_down);
}
