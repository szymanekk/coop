// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'Failure.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$Failure {
  String? get message => throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String? message) unauthenticated,
    required TResult Function(String? message) unknown,
    required TResult Function(String? message) connectionError,
    required TResult Function(String? message) newsletterError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String? message)? unauthenticated,
    TResult? Function(String? message)? unknown,
    TResult? Function(String? message)? connectionError,
    TResult? Function(String? message)? newsletterError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String? message)? unauthenticated,
    TResult Function(String? message)? unknown,
    TResult Function(String? message)? connectionError,
    TResult Function(String? message)? newsletterError,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(UnauthenticatedError value) unauthenticated,
    required TResult Function(UnknownError value) unknown,
    required TResult Function(ConnectionError value) connectionError,
    required TResult Function(NewsletterError value) newsletterError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(UnauthenticatedError value)? unauthenticated,
    TResult? Function(UnknownError value)? unknown,
    TResult? Function(ConnectionError value)? connectionError,
    TResult? Function(NewsletterError value)? newsletterError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(UnauthenticatedError value)? unauthenticated,
    TResult Function(UnknownError value)? unknown,
    TResult Function(ConnectionError value)? connectionError,
    TResult Function(NewsletterError value)? newsletterError,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $FailureCopyWith<Failure> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FailureCopyWith<$Res> {
  factory $FailureCopyWith(Failure value, $Res Function(Failure) then) =
      _$FailureCopyWithImpl<$Res, Failure>;
  @useResult
  $Res call({String? message});
}

/// @nodoc
class _$FailureCopyWithImpl<$Res, $Val extends Failure>
    implements $FailureCopyWith<$Res> {
  _$FailureCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? message = freezed,
  }) {
    return _then(_value.copyWith(
      message: freezed == message
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$UnauthenticatedErrorImplCopyWith<$Res>
    implements $FailureCopyWith<$Res> {
  factory _$$UnauthenticatedErrorImplCopyWith(_$UnauthenticatedErrorImpl value,
          $Res Function(_$UnauthenticatedErrorImpl) then) =
      __$$UnauthenticatedErrorImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String? message});
}

/// @nodoc
class __$$UnauthenticatedErrorImplCopyWithImpl<$Res>
    extends _$FailureCopyWithImpl<$Res, _$UnauthenticatedErrorImpl>
    implements _$$UnauthenticatedErrorImplCopyWith<$Res> {
  __$$UnauthenticatedErrorImplCopyWithImpl(_$UnauthenticatedErrorImpl _value,
      $Res Function(_$UnauthenticatedErrorImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? message = freezed,
  }) {
    return _then(_$UnauthenticatedErrorImpl(
      message: freezed == message
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc

class _$UnauthenticatedErrorImpl extends UnauthenticatedError {
  const _$UnauthenticatedErrorImpl({this.message}) : super._();

  @override
  final String? message;

  @override
  String toString() {
    return 'Failure.unauthenticated(message: $message)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$UnauthenticatedErrorImpl &&
            (identical(other.message, message) || other.message == message));
  }

  @override
  int get hashCode => Object.hash(runtimeType, message);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$UnauthenticatedErrorImplCopyWith<_$UnauthenticatedErrorImpl>
      get copyWith =>
          __$$UnauthenticatedErrorImplCopyWithImpl<_$UnauthenticatedErrorImpl>(
              this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String? message) unauthenticated,
    required TResult Function(String? message) unknown,
    required TResult Function(String? message) connectionError,
    required TResult Function(String? message) newsletterError,
  }) {
    return unauthenticated(message);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String? message)? unauthenticated,
    TResult? Function(String? message)? unknown,
    TResult? Function(String? message)? connectionError,
    TResult? Function(String? message)? newsletterError,
  }) {
    return unauthenticated?.call(message);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String? message)? unauthenticated,
    TResult Function(String? message)? unknown,
    TResult Function(String? message)? connectionError,
    TResult Function(String? message)? newsletterError,
    required TResult orElse(),
  }) {
    if (unauthenticated != null) {
      return unauthenticated(message);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(UnauthenticatedError value) unauthenticated,
    required TResult Function(UnknownError value) unknown,
    required TResult Function(ConnectionError value) connectionError,
    required TResult Function(NewsletterError value) newsletterError,
  }) {
    return unauthenticated(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(UnauthenticatedError value)? unauthenticated,
    TResult? Function(UnknownError value)? unknown,
    TResult? Function(ConnectionError value)? connectionError,
    TResult? Function(NewsletterError value)? newsletterError,
  }) {
    return unauthenticated?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(UnauthenticatedError value)? unauthenticated,
    TResult Function(UnknownError value)? unknown,
    TResult Function(ConnectionError value)? connectionError,
    TResult Function(NewsletterError value)? newsletterError,
    required TResult orElse(),
  }) {
    if (unauthenticated != null) {
      return unauthenticated(this);
    }
    return orElse();
  }
}

abstract class UnauthenticatedError extends Failure {
  const factory UnauthenticatedError({final String? message}) =
      _$UnauthenticatedErrorImpl;
  const UnauthenticatedError._() : super._();

  @override
  String? get message;
  @override
  @JsonKey(ignore: true)
  _$$UnauthenticatedErrorImplCopyWith<_$UnauthenticatedErrorImpl>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$UnknownErrorImplCopyWith<$Res>
    implements $FailureCopyWith<$Res> {
  factory _$$UnknownErrorImplCopyWith(
          _$UnknownErrorImpl value, $Res Function(_$UnknownErrorImpl) then) =
      __$$UnknownErrorImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String? message});
}

/// @nodoc
class __$$UnknownErrorImplCopyWithImpl<$Res>
    extends _$FailureCopyWithImpl<$Res, _$UnknownErrorImpl>
    implements _$$UnknownErrorImplCopyWith<$Res> {
  __$$UnknownErrorImplCopyWithImpl(
      _$UnknownErrorImpl _value, $Res Function(_$UnknownErrorImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? message = freezed,
  }) {
    return _then(_$UnknownErrorImpl(
      message: freezed == message
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc

class _$UnknownErrorImpl extends UnknownError {
  const _$UnknownErrorImpl({this.message}) : super._();

  @override
  final String? message;

  @override
  String toString() {
    return 'Failure.unknown(message: $message)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$UnknownErrorImpl &&
            (identical(other.message, message) || other.message == message));
  }

  @override
  int get hashCode => Object.hash(runtimeType, message);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$UnknownErrorImplCopyWith<_$UnknownErrorImpl> get copyWith =>
      __$$UnknownErrorImplCopyWithImpl<_$UnknownErrorImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String? message) unauthenticated,
    required TResult Function(String? message) unknown,
    required TResult Function(String? message) connectionError,
    required TResult Function(String? message) newsletterError,
  }) {
    return unknown(message);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String? message)? unauthenticated,
    TResult? Function(String? message)? unknown,
    TResult? Function(String? message)? connectionError,
    TResult? Function(String? message)? newsletterError,
  }) {
    return unknown?.call(message);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String? message)? unauthenticated,
    TResult Function(String? message)? unknown,
    TResult Function(String? message)? connectionError,
    TResult Function(String? message)? newsletterError,
    required TResult orElse(),
  }) {
    if (unknown != null) {
      return unknown(message);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(UnauthenticatedError value) unauthenticated,
    required TResult Function(UnknownError value) unknown,
    required TResult Function(ConnectionError value) connectionError,
    required TResult Function(NewsletterError value) newsletterError,
  }) {
    return unknown(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(UnauthenticatedError value)? unauthenticated,
    TResult? Function(UnknownError value)? unknown,
    TResult? Function(ConnectionError value)? connectionError,
    TResult? Function(NewsletterError value)? newsletterError,
  }) {
    return unknown?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(UnauthenticatedError value)? unauthenticated,
    TResult Function(UnknownError value)? unknown,
    TResult Function(ConnectionError value)? connectionError,
    TResult Function(NewsletterError value)? newsletterError,
    required TResult orElse(),
  }) {
    if (unknown != null) {
      return unknown(this);
    }
    return orElse();
  }
}

abstract class UnknownError extends Failure {
  const factory UnknownError({final String? message}) = _$UnknownErrorImpl;
  const UnknownError._() : super._();

  @override
  String? get message;
  @override
  @JsonKey(ignore: true)
  _$$UnknownErrorImplCopyWith<_$UnknownErrorImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$ConnectionErrorImplCopyWith<$Res>
    implements $FailureCopyWith<$Res> {
  factory _$$ConnectionErrorImplCopyWith(_$ConnectionErrorImpl value,
          $Res Function(_$ConnectionErrorImpl) then) =
      __$$ConnectionErrorImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String? message});
}

/// @nodoc
class __$$ConnectionErrorImplCopyWithImpl<$Res>
    extends _$FailureCopyWithImpl<$Res, _$ConnectionErrorImpl>
    implements _$$ConnectionErrorImplCopyWith<$Res> {
  __$$ConnectionErrorImplCopyWithImpl(
      _$ConnectionErrorImpl _value, $Res Function(_$ConnectionErrorImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? message = freezed,
  }) {
    return _then(_$ConnectionErrorImpl(
      message: freezed == message
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc

class _$ConnectionErrorImpl extends ConnectionError {
  const _$ConnectionErrorImpl({this.message}) : super._();

  @override
  final String? message;

  @override
  String toString() {
    return 'Failure.connectionError(message: $message)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ConnectionErrorImpl &&
            (identical(other.message, message) || other.message == message));
  }

  @override
  int get hashCode => Object.hash(runtimeType, message);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ConnectionErrorImplCopyWith<_$ConnectionErrorImpl> get copyWith =>
      __$$ConnectionErrorImplCopyWithImpl<_$ConnectionErrorImpl>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String? message) unauthenticated,
    required TResult Function(String? message) unknown,
    required TResult Function(String? message) connectionError,
    required TResult Function(String? message) newsletterError,
  }) {
    return connectionError(message);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String? message)? unauthenticated,
    TResult? Function(String? message)? unknown,
    TResult? Function(String? message)? connectionError,
    TResult? Function(String? message)? newsletterError,
  }) {
    return connectionError?.call(message);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String? message)? unauthenticated,
    TResult Function(String? message)? unknown,
    TResult Function(String? message)? connectionError,
    TResult Function(String? message)? newsletterError,
    required TResult orElse(),
  }) {
    if (connectionError != null) {
      return connectionError(message);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(UnauthenticatedError value) unauthenticated,
    required TResult Function(UnknownError value) unknown,
    required TResult Function(ConnectionError value) connectionError,
    required TResult Function(NewsletterError value) newsletterError,
  }) {
    return connectionError(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(UnauthenticatedError value)? unauthenticated,
    TResult? Function(UnknownError value)? unknown,
    TResult? Function(ConnectionError value)? connectionError,
    TResult? Function(NewsletterError value)? newsletterError,
  }) {
    return connectionError?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(UnauthenticatedError value)? unauthenticated,
    TResult Function(UnknownError value)? unknown,
    TResult Function(ConnectionError value)? connectionError,
    TResult Function(NewsletterError value)? newsletterError,
    required TResult orElse(),
  }) {
    if (connectionError != null) {
      return connectionError(this);
    }
    return orElse();
  }
}

abstract class ConnectionError extends Failure {
  const factory ConnectionError({final String? message}) =
      _$ConnectionErrorImpl;
  const ConnectionError._() : super._();

  @override
  String? get message;
  @override
  @JsonKey(ignore: true)
  _$$ConnectionErrorImplCopyWith<_$ConnectionErrorImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$NewsletterErrorImplCopyWith<$Res>
    implements $FailureCopyWith<$Res> {
  factory _$$NewsletterErrorImplCopyWith(_$NewsletterErrorImpl value,
          $Res Function(_$NewsletterErrorImpl) then) =
      __$$NewsletterErrorImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String? message});
}

/// @nodoc
class __$$NewsletterErrorImplCopyWithImpl<$Res>
    extends _$FailureCopyWithImpl<$Res, _$NewsletterErrorImpl>
    implements _$$NewsletterErrorImplCopyWith<$Res> {
  __$$NewsletterErrorImplCopyWithImpl(
      _$NewsletterErrorImpl _value, $Res Function(_$NewsletterErrorImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? message = freezed,
  }) {
    return _then(_$NewsletterErrorImpl(
      message: freezed == message
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc

class _$NewsletterErrorImpl extends NewsletterError {
  const _$NewsletterErrorImpl({this.message}) : super._();

  @override
  final String? message;

  @override
  String toString() {
    return 'Failure.newsletterError(message: $message)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$NewsletterErrorImpl &&
            (identical(other.message, message) || other.message == message));
  }

  @override
  int get hashCode => Object.hash(runtimeType, message);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$NewsletterErrorImplCopyWith<_$NewsletterErrorImpl> get copyWith =>
      __$$NewsletterErrorImplCopyWithImpl<_$NewsletterErrorImpl>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String? message) unauthenticated,
    required TResult Function(String? message) unknown,
    required TResult Function(String? message) connectionError,
    required TResult Function(String? message) newsletterError,
  }) {
    return newsletterError(message);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String? message)? unauthenticated,
    TResult? Function(String? message)? unknown,
    TResult? Function(String? message)? connectionError,
    TResult? Function(String? message)? newsletterError,
  }) {
    return newsletterError?.call(message);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String? message)? unauthenticated,
    TResult Function(String? message)? unknown,
    TResult Function(String? message)? connectionError,
    TResult Function(String? message)? newsletterError,
    required TResult orElse(),
  }) {
    if (newsletterError != null) {
      return newsletterError(message);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(UnauthenticatedError value) unauthenticated,
    required TResult Function(UnknownError value) unknown,
    required TResult Function(ConnectionError value) connectionError,
    required TResult Function(NewsletterError value) newsletterError,
  }) {
    return newsletterError(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(UnauthenticatedError value)? unauthenticated,
    TResult? Function(UnknownError value)? unknown,
    TResult? Function(ConnectionError value)? connectionError,
    TResult? Function(NewsletterError value)? newsletterError,
  }) {
    return newsletterError?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(UnauthenticatedError value)? unauthenticated,
    TResult Function(UnknownError value)? unknown,
    TResult Function(ConnectionError value)? connectionError,
    TResult Function(NewsletterError value)? newsletterError,
    required TResult orElse(),
  }) {
    if (newsletterError != null) {
      return newsletterError(this);
    }
    return orElse();
  }
}

abstract class NewsletterError extends Failure {
  const factory NewsletterError({final String? message}) =
      _$NewsletterErrorImpl;
  const NewsletterError._() : super._();

  @override
  String? get message;
  @override
  @JsonKey(ignore: true)
  _$$NewsletterErrorImplCopyWith<_$NewsletterErrorImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
