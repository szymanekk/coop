import 'package:freezed_annotation/freezed_annotation.dart';

part 'OpeningHoursEntity.freezed.dart';

@freezed
class OpeningHoursEntity with _$OpeningHoursEntity {
  factory OpeningHoursEntity({
    required String date,
    required String openHours,
    required String day,
    required bool isClosed,
    required bool isSpecialOpeningHours,
  }) = _OpeningHoursEntity;

  factory OpeningHoursEntity.fromDto(Map<String, dynamic> json) =>
      OpeningHoursEntity(
        date: json['Date'],
        openHours: json['OpenString'],
        day: json['Day'],
        isClosed: json['Closed'],
        isSpecialOpeningHours: json['SpecialOpeningHours']
      );

}

