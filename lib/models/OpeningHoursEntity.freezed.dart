// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'OpeningHoursEntity.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$OpeningHoursEntity {
  String get date => throw _privateConstructorUsedError;
  String get openHours => throw _privateConstructorUsedError;
  String get day => throw _privateConstructorUsedError;
  bool get isClosed => throw _privateConstructorUsedError;
  bool get isSpecialOpeningHours => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $OpeningHoursEntityCopyWith<OpeningHoursEntity> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $OpeningHoursEntityCopyWith<$Res> {
  factory $OpeningHoursEntityCopyWith(
          OpeningHoursEntity value, $Res Function(OpeningHoursEntity) then) =
      _$OpeningHoursEntityCopyWithImpl<$Res, OpeningHoursEntity>;
  @useResult
  $Res call(
      {String date,
      String openHours,
      String day,
      bool isClosed,
      bool isSpecialOpeningHours});
}

/// @nodoc
class _$OpeningHoursEntityCopyWithImpl<$Res, $Val extends OpeningHoursEntity>
    implements $OpeningHoursEntityCopyWith<$Res> {
  _$OpeningHoursEntityCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? date = null,
    Object? openHours = null,
    Object? day = null,
    Object? isClosed = null,
    Object? isSpecialOpeningHours = null,
  }) {
    return _then(_value.copyWith(
      date: null == date
          ? _value.date
          : date // ignore: cast_nullable_to_non_nullable
              as String,
      openHours: null == openHours
          ? _value.openHours
          : openHours // ignore: cast_nullable_to_non_nullable
              as String,
      day: null == day
          ? _value.day
          : day // ignore: cast_nullable_to_non_nullable
              as String,
      isClosed: null == isClosed
          ? _value.isClosed
          : isClosed // ignore: cast_nullable_to_non_nullable
              as bool,
      isSpecialOpeningHours: null == isSpecialOpeningHours
          ? _value.isSpecialOpeningHours
          : isSpecialOpeningHours // ignore: cast_nullable_to_non_nullable
              as bool,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$OpeningHoursEntityImplCopyWith<$Res>
    implements $OpeningHoursEntityCopyWith<$Res> {
  factory _$$OpeningHoursEntityImplCopyWith(_$OpeningHoursEntityImpl value,
          $Res Function(_$OpeningHoursEntityImpl) then) =
      __$$OpeningHoursEntityImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String date,
      String openHours,
      String day,
      bool isClosed,
      bool isSpecialOpeningHours});
}

/// @nodoc
class __$$OpeningHoursEntityImplCopyWithImpl<$Res>
    extends _$OpeningHoursEntityCopyWithImpl<$Res, _$OpeningHoursEntityImpl>
    implements _$$OpeningHoursEntityImplCopyWith<$Res> {
  __$$OpeningHoursEntityImplCopyWithImpl(_$OpeningHoursEntityImpl _value,
      $Res Function(_$OpeningHoursEntityImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? date = null,
    Object? openHours = null,
    Object? day = null,
    Object? isClosed = null,
    Object? isSpecialOpeningHours = null,
  }) {
    return _then(_$OpeningHoursEntityImpl(
      date: null == date
          ? _value.date
          : date // ignore: cast_nullable_to_non_nullable
              as String,
      openHours: null == openHours
          ? _value.openHours
          : openHours // ignore: cast_nullable_to_non_nullable
              as String,
      day: null == day
          ? _value.day
          : day // ignore: cast_nullable_to_non_nullable
              as String,
      isClosed: null == isClosed
          ? _value.isClosed
          : isClosed // ignore: cast_nullable_to_non_nullable
              as bool,
      isSpecialOpeningHours: null == isSpecialOpeningHours
          ? _value.isSpecialOpeningHours
          : isSpecialOpeningHours // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$OpeningHoursEntityImpl implements _OpeningHoursEntity {
  _$OpeningHoursEntityImpl(
      {required this.date,
      required this.openHours,
      required this.day,
      required this.isClosed,
      required this.isSpecialOpeningHours});

  @override
  final String date;
  @override
  final String openHours;
  @override
  final String day;
  @override
  final bool isClosed;
  @override
  final bool isSpecialOpeningHours;

  @override
  String toString() {
    return 'OpeningHoursEntity(date: $date, openHours: $openHours, day: $day, isClosed: $isClosed, isSpecialOpeningHours: $isSpecialOpeningHours)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$OpeningHoursEntityImpl &&
            (identical(other.date, date) || other.date == date) &&
            (identical(other.openHours, openHours) ||
                other.openHours == openHours) &&
            (identical(other.day, day) || other.day == day) &&
            (identical(other.isClosed, isClosed) ||
                other.isClosed == isClosed) &&
            (identical(other.isSpecialOpeningHours, isSpecialOpeningHours) ||
                other.isSpecialOpeningHours == isSpecialOpeningHours));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, date, openHours, day, isClosed, isSpecialOpeningHours);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$OpeningHoursEntityImplCopyWith<_$OpeningHoursEntityImpl> get copyWith =>
      __$$OpeningHoursEntityImplCopyWithImpl<_$OpeningHoursEntityImpl>(
          this, _$identity);
}

abstract class _OpeningHoursEntity implements OpeningHoursEntity {
  factory _OpeningHoursEntity(
      {required final String date,
      required final String openHours,
      required final String day,
      required final bool isClosed,
      required final bool isSpecialOpeningHours}) = _$OpeningHoursEntityImpl;

  @override
  String get date;
  @override
  String get openHours;
  @override
  String get day;
  @override
  bool get isClosed;
  @override
  bool get isSpecialOpeningHours;
  @override
  @JsonKey(ignore: true)
  _$$OpeningHoursEntityImplCopyWith<_$OpeningHoursEntityImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
