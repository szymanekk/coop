import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:latlong2/latlong.dart';

part 'RouteEntity.freezed.dart';

@freezed
class RouteEntity with _$RouteEntity {
  factory RouteEntity({
    required double distance,
    required List<LatLng> coordinates,
  }) = _RouteEntity;

  factory RouteEntity.fromDto(Map<String, dynamic> json) => RouteEntity(
      distance: json['distance'],
      coordinates: List<List<dynamic>>.from(
              (json['geometry'] as Map<String, dynamic>)['coordinates'])
          .map((e) => LatLng(e[1], e[0]))
          .toList());
}
