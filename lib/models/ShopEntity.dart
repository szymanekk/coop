import 'package:coop/models/OpeningHoursEntity.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:latlong2/latlong.dart';

part 'ShopEntity.freezed.dart';

@freezed
class ShopEntity with _$ShopEntity {
  factory ShopEntity({
    required String storeId,
    required LatLng location,
    required String name,
    required bool isEcommerce,
    required String newspaperUrl,
    required String openingHoursToday,
    required bool isOpenNow,
    required double distance,
    required List<OpeningHoursEntity> openingHours,
    required List<String> services,
     String? chainId,
     String? chain,
     String? chainClassName,
     String? chainImage,
     String? phone,
     String? address,
     String? city,
     String? email,
     String? organizationNumber,
     String? sLag,
  }) = _ShopEntity;

  factory ShopEntity.fromDto(Map<String, dynamic> json) => ShopEntity(
      storeId: json['StoreId'],
      name: json['Name'],
      chain: json['Chain'],
      chainClassName: json['ChainClassName'],
      chainId: json['ChainId'],
      isEcommerce: json['IsEcommerce'],
      newspaperUrl: json['NewspaperUrl'],
      chainImage: json['ChainImage'],
      location: LatLng(json['Lat'], json['Lng']),
      openingHoursToday: json['OpeningHoursToday'],
      isOpenNow: json['OpenNow'],
      phone: json['Phone'],
      address: json['Address'],
      city: json['City'],
      distance: double.tryParse(json['Distance'].toString()) ?? 0.0,
      email: json['Email'],
      sLag: json['SLag'],
      organizationNumber: json['OrganizationNumber'],
      openingHours: List<OpeningHoursEntity>.from(
          json['OpeningHours']
              .map((model) => OpeningHoursEntity.fromDto(model))),
      services: []);
}
