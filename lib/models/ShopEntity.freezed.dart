// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'ShopEntity.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$ShopEntity {
  String get storeId => throw _privateConstructorUsedError;
  LatLng get location => throw _privateConstructorUsedError;
  String get name => throw _privateConstructorUsedError;
  bool get isEcommerce => throw _privateConstructorUsedError;
  String get newspaperUrl => throw _privateConstructorUsedError;
  String get openingHoursToday => throw _privateConstructorUsedError;
  bool get isOpenNow => throw _privateConstructorUsedError;
  double get distance => throw _privateConstructorUsedError;
  List<OpeningHoursEntity> get openingHours =>
      throw _privateConstructorUsedError;
  List<String> get services => throw _privateConstructorUsedError;
  String? get chainId => throw _privateConstructorUsedError;
  String? get chain => throw _privateConstructorUsedError;
  String? get chainClassName => throw _privateConstructorUsedError;
  String? get chainImage => throw _privateConstructorUsedError;
  String? get phone => throw _privateConstructorUsedError;
  String? get address => throw _privateConstructorUsedError;
  String? get city => throw _privateConstructorUsedError;
  String? get email => throw _privateConstructorUsedError;
  String? get organizationNumber => throw _privateConstructorUsedError;
  String? get sLag => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $ShopEntityCopyWith<ShopEntity> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ShopEntityCopyWith<$Res> {
  factory $ShopEntityCopyWith(
          ShopEntity value, $Res Function(ShopEntity) then) =
      _$ShopEntityCopyWithImpl<$Res, ShopEntity>;
  @useResult
  $Res call(
      {String storeId,
      LatLng location,
      String name,
      bool isEcommerce,
      String newspaperUrl,
      String openingHoursToday,
      bool isOpenNow,
      double distance,
      List<OpeningHoursEntity> openingHours,
      List<String> services,
      String? chainId,
      String? chain,
      String? chainClassName,
      String? chainImage,
      String? phone,
      String? address,
      String? city,
      String? email,
      String? organizationNumber,
      String? sLag});
}

/// @nodoc
class _$ShopEntityCopyWithImpl<$Res, $Val extends ShopEntity>
    implements $ShopEntityCopyWith<$Res> {
  _$ShopEntityCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? storeId = null,
    Object? location = null,
    Object? name = null,
    Object? isEcommerce = null,
    Object? newspaperUrl = null,
    Object? openingHoursToday = null,
    Object? isOpenNow = null,
    Object? distance = null,
    Object? openingHours = null,
    Object? services = null,
    Object? chainId = freezed,
    Object? chain = freezed,
    Object? chainClassName = freezed,
    Object? chainImage = freezed,
    Object? phone = freezed,
    Object? address = freezed,
    Object? city = freezed,
    Object? email = freezed,
    Object? organizationNumber = freezed,
    Object? sLag = freezed,
  }) {
    return _then(_value.copyWith(
      storeId: null == storeId
          ? _value.storeId
          : storeId // ignore: cast_nullable_to_non_nullable
              as String,
      location: null == location
          ? _value.location
          : location // ignore: cast_nullable_to_non_nullable
              as LatLng,
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      isEcommerce: null == isEcommerce
          ? _value.isEcommerce
          : isEcommerce // ignore: cast_nullable_to_non_nullable
              as bool,
      newspaperUrl: null == newspaperUrl
          ? _value.newspaperUrl
          : newspaperUrl // ignore: cast_nullable_to_non_nullable
              as String,
      openingHoursToday: null == openingHoursToday
          ? _value.openingHoursToday
          : openingHoursToday // ignore: cast_nullable_to_non_nullable
              as String,
      isOpenNow: null == isOpenNow
          ? _value.isOpenNow
          : isOpenNow // ignore: cast_nullable_to_non_nullable
              as bool,
      distance: null == distance
          ? _value.distance
          : distance // ignore: cast_nullable_to_non_nullable
              as double,
      openingHours: null == openingHours
          ? _value.openingHours
          : openingHours // ignore: cast_nullable_to_non_nullable
              as List<OpeningHoursEntity>,
      services: null == services
          ? _value.services
          : services // ignore: cast_nullable_to_non_nullable
              as List<String>,
      chainId: freezed == chainId
          ? _value.chainId
          : chainId // ignore: cast_nullable_to_non_nullable
              as String?,
      chain: freezed == chain
          ? _value.chain
          : chain // ignore: cast_nullable_to_non_nullable
              as String?,
      chainClassName: freezed == chainClassName
          ? _value.chainClassName
          : chainClassName // ignore: cast_nullable_to_non_nullable
              as String?,
      chainImage: freezed == chainImage
          ? _value.chainImage
          : chainImage // ignore: cast_nullable_to_non_nullable
              as String?,
      phone: freezed == phone
          ? _value.phone
          : phone // ignore: cast_nullable_to_non_nullable
              as String?,
      address: freezed == address
          ? _value.address
          : address // ignore: cast_nullable_to_non_nullable
              as String?,
      city: freezed == city
          ? _value.city
          : city // ignore: cast_nullable_to_non_nullable
              as String?,
      email: freezed == email
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String?,
      organizationNumber: freezed == organizationNumber
          ? _value.organizationNumber
          : organizationNumber // ignore: cast_nullable_to_non_nullable
              as String?,
      sLag: freezed == sLag
          ? _value.sLag
          : sLag // ignore: cast_nullable_to_non_nullable
              as String?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$ShopEntityImplCopyWith<$Res>
    implements $ShopEntityCopyWith<$Res> {
  factory _$$ShopEntityImplCopyWith(
          _$ShopEntityImpl value, $Res Function(_$ShopEntityImpl) then) =
      __$$ShopEntityImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String storeId,
      LatLng location,
      String name,
      bool isEcommerce,
      String newspaperUrl,
      String openingHoursToday,
      bool isOpenNow,
      double distance,
      List<OpeningHoursEntity> openingHours,
      List<String> services,
      String? chainId,
      String? chain,
      String? chainClassName,
      String? chainImage,
      String? phone,
      String? address,
      String? city,
      String? email,
      String? organizationNumber,
      String? sLag});
}

/// @nodoc
class __$$ShopEntityImplCopyWithImpl<$Res>
    extends _$ShopEntityCopyWithImpl<$Res, _$ShopEntityImpl>
    implements _$$ShopEntityImplCopyWith<$Res> {
  __$$ShopEntityImplCopyWithImpl(
      _$ShopEntityImpl _value, $Res Function(_$ShopEntityImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? storeId = null,
    Object? location = null,
    Object? name = null,
    Object? isEcommerce = null,
    Object? newspaperUrl = null,
    Object? openingHoursToday = null,
    Object? isOpenNow = null,
    Object? distance = null,
    Object? openingHours = null,
    Object? services = null,
    Object? chainId = freezed,
    Object? chain = freezed,
    Object? chainClassName = freezed,
    Object? chainImage = freezed,
    Object? phone = freezed,
    Object? address = freezed,
    Object? city = freezed,
    Object? email = freezed,
    Object? organizationNumber = freezed,
    Object? sLag = freezed,
  }) {
    return _then(_$ShopEntityImpl(
      storeId: null == storeId
          ? _value.storeId
          : storeId // ignore: cast_nullable_to_non_nullable
              as String,
      location: null == location
          ? _value.location
          : location // ignore: cast_nullable_to_non_nullable
              as LatLng,
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      isEcommerce: null == isEcommerce
          ? _value.isEcommerce
          : isEcommerce // ignore: cast_nullable_to_non_nullable
              as bool,
      newspaperUrl: null == newspaperUrl
          ? _value.newspaperUrl
          : newspaperUrl // ignore: cast_nullable_to_non_nullable
              as String,
      openingHoursToday: null == openingHoursToday
          ? _value.openingHoursToday
          : openingHoursToday // ignore: cast_nullable_to_non_nullable
              as String,
      isOpenNow: null == isOpenNow
          ? _value.isOpenNow
          : isOpenNow // ignore: cast_nullable_to_non_nullable
              as bool,
      distance: null == distance
          ? _value.distance
          : distance // ignore: cast_nullable_to_non_nullable
              as double,
      openingHours: null == openingHours
          ? _value._openingHours
          : openingHours // ignore: cast_nullable_to_non_nullable
              as List<OpeningHoursEntity>,
      services: null == services
          ? _value._services
          : services // ignore: cast_nullable_to_non_nullable
              as List<String>,
      chainId: freezed == chainId
          ? _value.chainId
          : chainId // ignore: cast_nullable_to_non_nullable
              as String?,
      chain: freezed == chain
          ? _value.chain
          : chain // ignore: cast_nullable_to_non_nullable
              as String?,
      chainClassName: freezed == chainClassName
          ? _value.chainClassName
          : chainClassName // ignore: cast_nullable_to_non_nullable
              as String?,
      chainImage: freezed == chainImage
          ? _value.chainImage
          : chainImage // ignore: cast_nullable_to_non_nullable
              as String?,
      phone: freezed == phone
          ? _value.phone
          : phone // ignore: cast_nullable_to_non_nullable
              as String?,
      address: freezed == address
          ? _value.address
          : address // ignore: cast_nullable_to_non_nullable
              as String?,
      city: freezed == city
          ? _value.city
          : city // ignore: cast_nullable_to_non_nullable
              as String?,
      email: freezed == email
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String?,
      organizationNumber: freezed == organizationNumber
          ? _value.organizationNumber
          : organizationNumber // ignore: cast_nullable_to_non_nullable
              as String?,
      sLag: freezed == sLag
          ? _value.sLag
          : sLag // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc

class _$ShopEntityImpl implements _ShopEntity {
  _$ShopEntityImpl(
      {required this.storeId,
      required this.location,
      required this.name,
      required this.isEcommerce,
      required this.newspaperUrl,
      required this.openingHoursToday,
      required this.isOpenNow,
      required this.distance,
      required final List<OpeningHoursEntity> openingHours,
      required final List<String> services,
      this.chainId,
      this.chain,
      this.chainClassName,
      this.chainImage,
      this.phone,
      this.address,
      this.city,
      this.email,
      this.organizationNumber,
      this.sLag})
      : _openingHours = openingHours,
        _services = services;

  @override
  final String storeId;
  @override
  final LatLng location;
  @override
  final String name;
  @override
  final bool isEcommerce;
  @override
  final String newspaperUrl;
  @override
  final String openingHoursToday;
  @override
  final bool isOpenNow;
  @override
  final double distance;
  final List<OpeningHoursEntity> _openingHours;
  @override
  List<OpeningHoursEntity> get openingHours {
    if (_openingHours is EqualUnmodifiableListView) return _openingHours;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_openingHours);
  }

  final List<String> _services;
  @override
  List<String> get services {
    if (_services is EqualUnmodifiableListView) return _services;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_services);
  }

  @override
  final String? chainId;
  @override
  final String? chain;
  @override
  final String? chainClassName;
  @override
  final String? chainImage;
  @override
  final String? phone;
  @override
  final String? address;
  @override
  final String? city;
  @override
  final String? email;
  @override
  final String? organizationNumber;
  @override
  final String? sLag;

  @override
  String toString() {
    return 'ShopEntity(storeId: $storeId, location: $location, name: $name, isEcommerce: $isEcommerce, newspaperUrl: $newspaperUrl, openingHoursToday: $openingHoursToday, isOpenNow: $isOpenNow, distance: $distance, openingHours: $openingHours, services: $services, chainId: $chainId, chain: $chain, chainClassName: $chainClassName, chainImage: $chainImage, phone: $phone, address: $address, city: $city, email: $email, organizationNumber: $organizationNumber, sLag: $sLag)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ShopEntityImpl &&
            (identical(other.storeId, storeId) || other.storeId == storeId) &&
            (identical(other.location, location) ||
                other.location == location) &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.isEcommerce, isEcommerce) ||
                other.isEcommerce == isEcommerce) &&
            (identical(other.newspaperUrl, newspaperUrl) ||
                other.newspaperUrl == newspaperUrl) &&
            (identical(other.openingHoursToday, openingHoursToday) ||
                other.openingHoursToday == openingHoursToday) &&
            (identical(other.isOpenNow, isOpenNow) ||
                other.isOpenNow == isOpenNow) &&
            (identical(other.distance, distance) ||
                other.distance == distance) &&
            const DeepCollectionEquality()
                .equals(other._openingHours, _openingHours) &&
            const DeepCollectionEquality().equals(other._services, _services) &&
            (identical(other.chainId, chainId) || other.chainId == chainId) &&
            (identical(other.chain, chain) || other.chain == chain) &&
            (identical(other.chainClassName, chainClassName) ||
                other.chainClassName == chainClassName) &&
            (identical(other.chainImage, chainImage) ||
                other.chainImage == chainImage) &&
            (identical(other.phone, phone) || other.phone == phone) &&
            (identical(other.address, address) || other.address == address) &&
            (identical(other.city, city) || other.city == city) &&
            (identical(other.email, email) || other.email == email) &&
            (identical(other.organizationNumber, organizationNumber) ||
                other.organizationNumber == organizationNumber) &&
            (identical(other.sLag, sLag) || other.sLag == sLag));
  }

  @override
  int get hashCode => Object.hashAll([
        runtimeType,
        storeId,
        location,
        name,
        isEcommerce,
        newspaperUrl,
        openingHoursToday,
        isOpenNow,
        distance,
        const DeepCollectionEquality().hash(_openingHours),
        const DeepCollectionEquality().hash(_services),
        chainId,
        chain,
        chainClassName,
        chainImage,
        phone,
        address,
        city,
        email,
        organizationNumber,
        sLag
      ]);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ShopEntityImplCopyWith<_$ShopEntityImpl> get copyWith =>
      __$$ShopEntityImplCopyWithImpl<_$ShopEntityImpl>(this, _$identity);
}

abstract class _ShopEntity implements ShopEntity {
  factory _ShopEntity(
      {required final String storeId,
      required final LatLng location,
      required final String name,
      required final bool isEcommerce,
      required final String newspaperUrl,
      required final String openingHoursToday,
      required final bool isOpenNow,
      required final double distance,
      required final List<OpeningHoursEntity> openingHours,
      required final List<String> services,
      final String? chainId,
      final String? chain,
      final String? chainClassName,
      final String? chainImage,
      final String? phone,
      final String? address,
      final String? city,
      final String? email,
      final String? organizationNumber,
      final String? sLag}) = _$ShopEntityImpl;

  @override
  String get storeId;
  @override
  LatLng get location;
  @override
  String get name;
  @override
  bool get isEcommerce;
  @override
  String get newspaperUrl;
  @override
  String get openingHoursToday;
  @override
  bool get isOpenNow;
  @override
  double get distance;
  @override
  List<OpeningHoursEntity> get openingHours;
  @override
  List<String> get services;
  @override
  String? get chainId;
  @override
  String? get chain;
  @override
  String? get chainClassName;
  @override
  String? get chainImage;
  @override
  String? get phone;
  @override
  String? get address;
  @override
  String? get city;
  @override
  String? get email;
  @override
  String? get organizationNumber;
  @override
  String? get sLag;
  @override
  @JsonKey(ignore: true)
  _$$ShopEntityImplCopyWith<_$ShopEntityImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
