import 'package:coop/modules/map/MapPage.dart';
import 'package:flutter/material.dart';
import 'package:coop/generated/l10n.dart';

class SplashPage extends StatelessWidget {
  static const routeName = '/';

  const SplashPage({super.key});

  @override
  Widget build(BuildContext context) => const SplashPageView();
}

class SplashPageView extends StatefulWidget {
  const SplashPageView({super.key});

  @override
  State<StatefulWidget> createState() => SplashPageState();
}

class SplashPageState extends State<SplashPageView> {
  final _duration = 300;

  @override
  initState() {
    super.initState();

    Future.delayed(Duration(milliseconds: _duration)).then((value) =>
        Navigator.of(context).pushReplacementNamed(MapPage.routeName));
  }

  Widget pageContent(BuildContext context) => Scaffold(
        body: Center(
          child: Text(
            S.of(context).coop,
            style: Theme.of(context).textTheme.displayMedium,
          ),
        ),
      );

  @override
  Widget build(BuildContext context) => pageContent(context);
}
