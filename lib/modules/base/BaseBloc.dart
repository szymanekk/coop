import 'dart:async';

import 'package:dio/dio.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:coop/models/Failure.dart';
import 'BaseEvent.dart';
import 'BaseState.dart';

abstract class BaseBloc<BlocEvent extends BaseEvent,
    BlocState extends BaseState> extends Bloc<BlocEvent, BlocState> {
  BaseBloc(super.initialState);

  void onCatchError(Emitter<BaseState> emitter, Failure failure);

  handleEvent({
    required Emitter<BaseState> emitter,
    required Future<void> Function(SuccessState state) onFunction,
  }) async {
    try {
      if (state is SuccessState) {
        await onFunction.call(state as SuccessState);
      }
    } catch (ex) {
      onCatchError(emitter, Failure.fromException(error: ex));
    }
  }
}
