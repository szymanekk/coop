import 'package:coop/models/Failure.dart';
import 'package:coop/modules/base/BaseBloc.dart';
import 'package:coop/ui/ErrorPageWidget.dart';
import 'package:coop/ui/LoadingPageWidget.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:coop/generated/l10n.dart';
import 'package:coop/modules/base/BaseEvent.dart';
import 'package:coop/modules/base/BaseState.dart';

abstract class BasePageStateWidget<
    Bloc extends BaseBloc<BlocEvent, BlocState>,
    BlocEvent extends BaseEvent,
    BlocState extends BaseState,
    BlocSuccessState extends SuccessState,
    BlocErrorState extends ErrorState,
    Argument> extends State<StatefulWidget> {
  @override
  @mustCallSuper
  void initState() {
    super.initState();

    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      onPageOpened();
    });
  }

  void onPageOpened(){}

  void onRefresh(){}

  Argument get argument =>
      ModalRoute.of(context)!.settings.arguments as Argument;

  S get strings => S.of(context);

  ThemeData get theme => Theme.of(context);

  PreferredSizeWidget? get appBar => null;

  Widget? get floatingButtonWidget => null;

  Widget? get content => null;

  Widget successContent(BlocSuccessState state) => Container();

  void listener(BuildContext context, BaseState state) {}

  Widget _error(Failure error) => ErrorPageWidget(
        failure: error,
        onRefresh: () => onRefresh(),
      );

  @override
  Widget build(BuildContext context) =>
      BlocConsumer<Bloc, BlocState>(builder: (context, state) {
        if (state is ErrorState) {
          return _error(state.error);
        }

        if (state is SuccessState) {
          return Scaffold(
            appBar: appBar,
            floatingActionButton: floatingButtonWidget,
            body: successContent(state as BlocSuccessState),
          );
        }

        if (state is LoadingState) {
          return const LoadingWidget();
        }

        return _error(const Failure.unknown());
      }, listener: (BuildContext context, BlocState state) {
        if (state is BlocErrorState) {
          Future.delayed(const Duration(seconds: 2))
              .then((value) {
            if(mounted){
              onPageOpened();
            }
          });
        } else {
          listener(context, state);
        }
      });
}
