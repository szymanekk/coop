import 'package:coop/models/Failure.dart';

abstract class BaseState {}

abstract class ErrorState extends BaseState {
  Failure get error;
}

abstract class LoadingState extends BaseState {}

abstract class SuccessState extends BaseState {}