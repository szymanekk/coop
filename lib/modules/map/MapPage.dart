import 'package:coop/models/ShopEntity.dart';
import 'package:coop/modules/map/bloc/MapBloc.dart';
import 'package:coop/modules/map/bloc/MapBlocEvent.dart';
import 'package:coop/modules/map/bloc/MapBlocState.dart';
import 'package:coop/modules/shop/ShopPage.dart';
import 'package:coop/resources/AppApis.dart';
import 'package:coop/resources/AppColors.dart';
import 'package:coop/resources/AppConstants.dart';
import 'package:coop/ui/AlertDialogWidget.dart';
import 'package:coop/modules/base/BasePageWidget.dart';
import 'package:coop/ui/ButtonWidget.dart';
import 'package:coop/ui/CircleButtonWidget.dart';
import 'package:coop/ui/TextInputWidget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:coop/Di.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:coop/modules/base/BaseState.dart';

class MapPage extends StatelessWidget {
  static const routeName = '/map';

  const MapPage({super.key});

  @override
  Widget build(BuildContext context) => BlocProvider<MapBloc>(
        create: (BuildContext context) => MapBloc(getIt(), getIt()),
        child: const MapPageWidget(),
      );
}

class MapPageWidget extends StatefulWidget {
  const MapPageWidget({super.key});

  @override
  MapPageState createState() => MapPageState();
}

class MapPageState extends BasePageStateWidget<MapBloc, MapBlocEvent,
    MapBlocState, SuccessPage, ErrorPage, void> {
  final MapController mapController = MapController();


  @override
  void onPageOpened() => context.read<MapBloc>().add(PageOpened());

  @override
  void onRefresh()  =>
    context.read<MapBloc>().add(PageOpened());

  @override
  void dispose() {
    mapController.dispose();
    super.dispose();
  }

  Marker currentLocationMarker(SuccessPage state) => Marker(
      point: state.currentLocation,
      child: const Icon(
        Icons.circle,
        size: UiConstant.defaultMarkerIcon,
        color: AppColors.yellow,
      ));

  Marker shopMarker(ShopEntity shop, ShopEntity? current) => Marker(
        point: shop.location,
        child: CircleButtonWidget(
          onPressed: () => context.read<MapBloc>().add(SelectShop(shop: shop)),
          bgColor: shop == current
              ? Theme.of(context).primaryColor
              : Theme.of(context).colorScheme.secondary.withOpacity(0.9),
          innerPadding: UiConstant.defaultMarkerPadding,
          iconSize: UiConstant.defaultMarkerIcon,
          icon: Icons.shopping_basket,
        ),
      );

  Widget map(SuccessPage state) => FlutterMap(
        mapController: mapController,
        options: MapOptions(
          initialCenter: state.currentLocation,
          initialZoom: state.currentZoom,
        ),
        children: [
          TileLayer(
            urlTemplate: AppApis.map,
          ),
          if (state.showPath && state.route != null)
            PolylineLayer(
              polylines: [
                Polyline(
                  points: state.route!.coordinates,
                  color: AppColors.yellow.withOpacity(0.8),
                  strokeWidth: 3.0,
                ),
              ],
            ),
          MarkerLayer(markers: [
            ...state.filteredShops
                .map((e) => shopMarker(e, state.selectedShop))
                .toList(),
            currentLocationMarker(state)
          ]),
        ],
      );

  Widget searchArea() => Card(
        color: theme.cardColor,
        child: Padding(
          padding: const EdgeInsets.all(UiConstant.cardPadding),
          child: Row(
            children: [
              Expanded(
                  child: TextInputWidget(
                hint: strings.searchByPost,
              )),
              const SizedBox(
                width: UiConstant.baseTextSpace,
              ),
              CircleButtonWidget(
                  icon: Icons.search,
                  onPressed: () =>
                      context.read<MapBloc>().add(SearchByPostNumber())),
            ],
          ),
        ),
      );

  Widget selectedShop(SuccessPage state, ShopEntity entity) => Card(
        color: theme.cardColor,
        child: Padding(
          padding: const EdgeInsets.all(UiConstant.cardPadding),
          child: Column(
            children: [
              Row(
                children: [
                  Expanded(child: Text(entity.name)),
                  IconButton(
                      onPressed: () =>
                          context.read<MapBloc>().add(SelectShop()),
                      icon: const Icon(Icons.close)),
                ],
              ),
              Row(
                children: [
                  Expanded(
                    child: details(entity),
                  ),
                  const SizedBox(
                    width: UiConstant.baseTextSpace,
                  ),
                  Expanded(
                    child: state.showPath
                        ? stopNavigation(entity)
                        : navigate(entity),
                  ),
                ],
              )
            ],
          ),
        ),
      );

  Widget details(ShopEntity entity) => ButtonWidget.light(
      onPressed: () => Navigator.of(context)
          .pushNamed(ShopPage.routeName, arguments: entity),
      label: strings.openDetails);

  Widget navigate(ShopEntity entity) => ButtonWidget.dark(
      onPressed: () => context.read<MapBloc>().add(StartNavigation()),
      label: strings.navigate);

  Widget stopNavigation(ShopEntity entity) => ButtonWidget.dark(
      onPressed: () => context.read<MapBloc>().add(StopNavigation()),
      label: strings.stopNavigation);

  Widget header(SuccessPage state) => SafeArea(
        child: Padding(
            padding: const EdgeInsets.all(UiConstant.cardPadding),
            child: Column(
              children: [
                searchArea(),
                if (state.selectedShop != null)
                  selectedShop(state, state.selectedShop!)
              ],
            )),
      );

  Widget locationButton() => CircleButtonWidget(
      onPressed: () => context.read<MapBloc>().add(GetCurrentLocation()),
      icon: Icons.my_location_rounded);

  @override
  Widget successContent(SuccessPage state) => Stack(
        children: [
          map(state),
          header(state),
        ],
      );

  @override
  Widget? get floatingButtonWidget => FloatingActionButton(
        onPressed: () => context.read<MapBloc>().add(GetCurrentLocation()),
        child: const Icon(Icons.my_location_rounded),
      );

  @override
  void listener(BuildContext context, BaseState state) {
    if (state is SuccessPage && state.shops.isEmpty) {
      AlertDialogWidget.show(context,
          title: strings.noShops, bodyText: strings.noShopsDescription);
    }
  }

}
