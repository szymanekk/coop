import 'dart:async';

import 'package:coop/models/Extensions.dart';
import 'package:coop/models/Failure.dart';
import 'package:coop/modules/base/BaseBloc.dart';
import 'package:coop/modules/base/BaseState.dart';
import 'package:coop/utils/PermissionUtils.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:coop/repositories/ShopRepositoryBase.dart';
import 'package:geolocator/geolocator.dart';
import 'package:latlong2/latlong.dart';
import 'package:coop/resources/AppConstants.dart';
import 'package:coop/repositories/RouteRepositoryBase.dart';
import 'MapBlocEvent.dart';
import 'MapBlocState.dart';

class MapBloc extends BaseBloc<MapBlocEvent, MapBlocState> {
  final ShopRepositoryBase shopRepository;
  final RouteRepositoryBase routeRepository;

  MapBloc(this.shopRepository, this.routeRepository)
      : super(const LoadingPage()) {
    on<PageOpened>((event, emit) => onPageOpened(emit));
    on<GetCurrentLocation>(getCurrentLocation);
    on<SearchByPostNumber>(searchByPostNumber);
    on<SelectShop>(selectShop);
    on<StartNavigation>(startNavigation);
    on<StopNavigation>(stopNavigation);
    on<UpdateCurrentLocation>(updateCurrentLocation);
  }

  late StreamSubscription<Position> _positionStreamSubscription;
  final geolocator = GeolocatorPlatform.instance;

  @override
  Future<void> close() {
    _positionStreamSubscription.cancel();
    return super.close();
  }

  void updateCurrentLocation(
    UpdateCurrentLocation event,
    Emitter<MapBlocState> emit,
  ) =>
      handleEvent(emitter: emit, onFunction: (currentState) async {
        final current = currentState as SuccessPage;
        final route = current.showPath && current.selectedShop != null
            ? await routeRepository.getRoute(
                currentLocation: event.location,
                shopLocation: current.selectedShop!.location)
            : null;

        emit(current.copyWith(currentLocation: event.location, route: route));
      });

  void selectShop(
    SelectShop event,
    Emitter<MapBlocState> emit,
  ) =>
      handleEvent(emitter: emit, onFunction: (currentState) async {
        final current = currentState as SuccessPage;
        _positionStreamSubscription.pause();
        emit(current.copyWith(selectedShop: event.shop, showPath: false));
      });

  void stopNavigation(
    StopNavigation event,
    Emitter<MapBlocState> emit,
  ) =>
      handleEvent(emitter: emit, onFunction: (currentState) async {
        final current = currentState as SuccessPage;
        _positionStreamSubscription.pause();
        emit(current.copyWith(showPath: false, route: null));
      });

  void startNavigation(
    StartNavigation event,
    Emitter<MapBlocState> emit,
  ) =>
      handleEvent(emitter: emit, onFunction: (currentState) async {
        final current = currentState as SuccessPage;
        add(UpdateCurrentLocation(current.currentLocation));
        emit(current.copyWith(showPath: true));
        _positionStreamSubscription.resume();
      });

  void searchByPostNumber(
    SearchByPostNumber event,
    Emitter<MapBlocState> emit,
  ) =>
      handleEvent(emitter: emit, onFunction: (currentState) async {
        final current = currentState as SuccessPage;
        final shops = await shopRepository.getShopsByPostNumber(
            postNumber: current.search);
        emit(current.copyWith(shops: shops, selectedShop: null));
      });

  void getCurrentLocation(
    GetCurrentLocation event,
    Emitter<MapBlocState> emit,
  ) =>
      handleEvent(emitter: emit, onFunction: (currentState) async {
        final current = currentState as SuccessPage;
        final getLocationPermission = await PermissionUtils.handlePermission();
        if (getLocationPermission) {
          final curPosition =
              await GeolocatorPlatform.instance.getCurrentPosition();
          final location = LatLng(curPosition.latitude, curPosition.longitude);

          final shops =
              await shopRepository.getShopsByLocation(location: location);
          emit(current.copyWith(
              currentLocation: location, shops: shops, selectedShop: null));
        }
      });

  Future<void> onPageOpened(
    Emitter<MapBlocState> emit,
  ) async {
    try{
      final getLocationPermission = await PermissionUtils.handlePermission();
      late LatLng currentLocation;
      if (getLocationPermission) {
        final curPosition = await geolocator.getCurrentPosition()
            .timeout(const Duration(seconds: 5));
        currentLocation = LatLng(curPosition.latitude, curPosition.longitude);
      } else {
        currentLocation = ConstantLocations.osloLocation;
      }
      final shops =
      await shopRepository.getShopsByLocation(location: currentLocation);

      emit(SuccessPage(
          currentLocation: currentLocation,
          currentZoom: 12,
          shops: shops,
          showPath: false,
          search: ''));

      _positionStreamSubscription = Geolocator.getPositionStream(
          locationSettings: const LocationSettings(
              accuracy: LocationAccuracy.bestForNavigation))
          .listen((Position position) {
        add(UpdateCurrentLocation(position.latlng));
      });
      _positionStreamSubscription.pause();
    } catch(ex){
      emit(ErrorPage(error: Failure.fromException(error: ex)));
    }
  }

  @override
  void onCatchError(Emitter<BaseState> emitter, Failure failure) {
    emitter(ErrorPage(error: failure));
  }
}
