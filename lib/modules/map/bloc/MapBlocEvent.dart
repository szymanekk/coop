import 'package:coop/models/ShopEntity.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:latlong2/latlong.dart';
import 'package:coop/modules/base/BaseEvent.dart';

part 'MapBlocEvent.freezed.dart';

@freezed
class MapBlocEvent with _$MapBlocEvent implements BaseEvent{
  factory MapBlocEvent.pageOpened() = PageOpened;
  factory MapBlocEvent.getCurrentLocation() = GetCurrentLocation;
  factory MapBlocEvent.searchByPostNumber() = SearchByPostNumber;
  factory MapBlocEvent.onPostNumberChange(String value) = OnPostNumberChange;
  factory MapBlocEvent.startNavigation() = StartNavigation;
  factory MapBlocEvent.stopNavigation() = StopNavigation;
  factory MapBlocEvent.selectRobot({ShopEntity? shop}) = SelectShop;
  factory MapBlocEvent.updateCurrentLocation(LatLng location) = UpdateCurrentLocation;
}
