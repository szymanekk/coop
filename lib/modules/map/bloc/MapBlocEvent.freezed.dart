// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'MapBlocEvent.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$MapBlocEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() pageOpened,
    required TResult Function() getCurrentLocation,
    required TResult Function() searchByPostNumber,
    required TResult Function(String value) onPostNumberChange,
    required TResult Function() startNavigation,
    required TResult Function() stopNavigation,
    required TResult Function(ShopEntity? shop) selectRobot,
    required TResult Function(LatLng location) updateCurrentLocation,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? pageOpened,
    TResult? Function()? getCurrentLocation,
    TResult? Function()? searchByPostNumber,
    TResult? Function(String value)? onPostNumberChange,
    TResult? Function()? startNavigation,
    TResult? Function()? stopNavigation,
    TResult? Function(ShopEntity? shop)? selectRobot,
    TResult? Function(LatLng location)? updateCurrentLocation,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? pageOpened,
    TResult Function()? getCurrentLocation,
    TResult Function()? searchByPostNumber,
    TResult Function(String value)? onPostNumberChange,
    TResult Function()? startNavigation,
    TResult Function()? stopNavigation,
    TResult Function(ShopEntity? shop)? selectRobot,
    TResult Function(LatLng location)? updateCurrentLocation,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(PageOpened value) pageOpened,
    required TResult Function(GetCurrentLocation value) getCurrentLocation,
    required TResult Function(SearchByPostNumber value) searchByPostNumber,
    required TResult Function(OnPostNumberChange value) onPostNumberChange,
    required TResult Function(StartNavigation value) startNavigation,
    required TResult Function(StopNavigation value) stopNavigation,
    required TResult Function(SelectShop value) selectRobot,
    required TResult Function(UpdateCurrentLocation value)
        updateCurrentLocation,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(PageOpened value)? pageOpened,
    TResult? Function(GetCurrentLocation value)? getCurrentLocation,
    TResult? Function(SearchByPostNumber value)? searchByPostNumber,
    TResult? Function(OnPostNumberChange value)? onPostNumberChange,
    TResult? Function(StartNavigation value)? startNavigation,
    TResult? Function(StopNavigation value)? stopNavigation,
    TResult? Function(SelectShop value)? selectRobot,
    TResult? Function(UpdateCurrentLocation value)? updateCurrentLocation,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(PageOpened value)? pageOpened,
    TResult Function(GetCurrentLocation value)? getCurrentLocation,
    TResult Function(SearchByPostNumber value)? searchByPostNumber,
    TResult Function(OnPostNumberChange value)? onPostNumberChange,
    TResult Function(StartNavigation value)? startNavigation,
    TResult Function(StopNavigation value)? stopNavigation,
    TResult Function(SelectShop value)? selectRobot,
    TResult Function(UpdateCurrentLocation value)? updateCurrentLocation,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $MapBlocEventCopyWith<$Res> {
  factory $MapBlocEventCopyWith(
          MapBlocEvent value, $Res Function(MapBlocEvent) then) =
      _$MapBlocEventCopyWithImpl<$Res, MapBlocEvent>;
}

/// @nodoc
class _$MapBlocEventCopyWithImpl<$Res, $Val extends MapBlocEvent>
    implements $MapBlocEventCopyWith<$Res> {
  _$MapBlocEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$PageOpenedImplCopyWith<$Res> {
  factory _$$PageOpenedImplCopyWith(
          _$PageOpenedImpl value, $Res Function(_$PageOpenedImpl) then) =
      __$$PageOpenedImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$PageOpenedImplCopyWithImpl<$Res>
    extends _$MapBlocEventCopyWithImpl<$Res, _$PageOpenedImpl>
    implements _$$PageOpenedImplCopyWith<$Res> {
  __$$PageOpenedImplCopyWithImpl(
      _$PageOpenedImpl _value, $Res Function(_$PageOpenedImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$PageOpenedImpl implements PageOpened {
  _$PageOpenedImpl();

  @override
  String toString() {
    return 'MapBlocEvent.pageOpened()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$PageOpenedImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() pageOpened,
    required TResult Function() getCurrentLocation,
    required TResult Function() searchByPostNumber,
    required TResult Function(String value) onPostNumberChange,
    required TResult Function() startNavigation,
    required TResult Function() stopNavigation,
    required TResult Function(ShopEntity? shop) selectRobot,
    required TResult Function(LatLng location) updateCurrentLocation,
  }) {
    return pageOpened();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? pageOpened,
    TResult? Function()? getCurrentLocation,
    TResult? Function()? searchByPostNumber,
    TResult? Function(String value)? onPostNumberChange,
    TResult? Function()? startNavigation,
    TResult? Function()? stopNavigation,
    TResult? Function(ShopEntity? shop)? selectRobot,
    TResult? Function(LatLng location)? updateCurrentLocation,
  }) {
    return pageOpened?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? pageOpened,
    TResult Function()? getCurrentLocation,
    TResult Function()? searchByPostNumber,
    TResult Function(String value)? onPostNumberChange,
    TResult Function()? startNavigation,
    TResult Function()? stopNavigation,
    TResult Function(ShopEntity? shop)? selectRobot,
    TResult Function(LatLng location)? updateCurrentLocation,
    required TResult orElse(),
  }) {
    if (pageOpened != null) {
      return pageOpened();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(PageOpened value) pageOpened,
    required TResult Function(GetCurrentLocation value) getCurrentLocation,
    required TResult Function(SearchByPostNumber value) searchByPostNumber,
    required TResult Function(OnPostNumberChange value) onPostNumberChange,
    required TResult Function(StartNavigation value) startNavigation,
    required TResult Function(StopNavigation value) stopNavigation,
    required TResult Function(SelectShop value) selectRobot,
    required TResult Function(UpdateCurrentLocation value)
        updateCurrentLocation,
  }) {
    return pageOpened(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(PageOpened value)? pageOpened,
    TResult? Function(GetCurrentLocation value)? getCurrentLocation,
    TResult? Function(SearchByPostNumber value)? searchByPostNumber,
    TResult? Function(OnPostNumberChange value)? onPostNumberChange,
    TResult? Function(StartNavigation value)? startNavigation,
    TResult? Function(StopNavigation value)? stopNavigation,
    TResult? Function(SelectShop value)? selectRobot,
    TResult? Function(UpdateCurrentLocation value)? updateCurrentLocation,
  }) {
    return pageOpened?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(PageOpened value)? pageOpened,
    TResult Function(GetCurrentLocation value)? getCurrentLocation,
    TResult Function(SearchByPostNumber value)? searchByPostNumber,
    TResult Function(OnPostNumberChange value)? onPostNumberChange,
    TResult Function(StartNavigation value)? startNavigation,
    TResult Function(StopNavigation value)? stopNavigation,
    TResult Function(SelectShop value)? selectRobot,
    TResult Function(UpdateCurrentLocation value)? updateCurrentLocation,
    required TResult orElse(),
  }) {
    if (pageOpened != null) {
      return pageOpened(this);
    }
    return orElse();
  }
}

abstract class PageOpened implements MapBlocEvent {
  factory PageOpened() = _$PageOpenedImpl;
}

/// @nodoc
abstract class _$$GetCurrentLocationImplCopyWith<$Res> {
  factory _$$GetCurrentLocationImplCopyWith(_$GetCurrentLocationImpl value,
          $Res Function(_$GetCurrentLocationImpl) then) =
      __$$GetCurrentLocationImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$GetCurrentLocationImplCopyWithImpl<$Res>
    extends _$MapBlocEventCopyWithImpl<$Res, _$GetCurrentLocationImpl>
    implements _$$GetCurrentLocationImplCopyWith<$Res> {
  __$$GetCurrentLocationImplCopyWithImpl(_$GetCurrentLocationImpl _value,
      $Res Function(_$GetCurrentLocationImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$GetCurrentLocationImpl implements GetCurrentLocation {
  _$GetCurrentLocationImpl();

  @override
  String toString() {
    return 'MapBlocEvent.getCurrentLocation()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$GetCurrentLocationImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() pageOpened,
    required TResult Function() getCurrentLocation,
    required TResult Function() searchByPostNumber,
    required TResult Function(String value) onPostNumberChange,
    required TResult Function() startNavigation,
    required TResult Function() stopNavigation,
    required TResult Function(ShopEntity? shop) selectRobot,
    required TResult Function(LatLng location) updateCurrentLocation,
  }) {
    return getCurrentLocation();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? pageOpened,
    TResult? Function()? getCurrentLocation,
    TResult? Function()? searchByPostNumber,
    TResult? Function(String value)? onPostNumberChange,
    TResult? Function()? startNavigation,
    TResult? Function()? stopNavigation,
    TResult? Function(ShopEntity? shop)? selectRobot,
    TResult? Function(LatLng location)? updateCurrentLocation,
  }) {
    return getCurrentLocation?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? pageOpened,
    TResult Function()? getCurrentLocation,
    TResult Function()? searchByPostNumber,
    TResult Function(String value)? onPostNumberChange,
    TResult Function()? startNavigation,
    TResult Function()? stopNavigation,
    TResult Function(ShopEntity? shop)? selectRobot,
    TResult Function(LatLng location)? updateCurrentLocation,
    required TResult orElse(),
  }) {
    if (getCurrentLocation != null) {
      return getCurrentLocation();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(PageOpened value) pageOpened,
    required TResult Function(GetCurrentLocation value) getCurrentLocation,
    required TResult Function(SearchByPostNumber value) searchByPostNumber,
    required TResult Function(OnPostNumberChange value) onPostNumberChange,
    required TResult Function(StartNavigation value) startNavigation,
    required TResult Function(StopNavigation value) stopNavigation,
    required TResult Function(SelectShop value) selectRobot,
    required TResult Function(UpdateCurrentLocation value)
        updateCurrentLocation,
  }) {
    return getCurrentLocation(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(PageOpened value)? pageOpened,
    TResult? Function(GetCurrentLocation value)? getCurrentLocation,
    TResult? Function(SearchByPostNumber value)? searchByPostNumber,
    TResult? Function(OnPostNumberChange value)? onPostNumberChange,
    TResult? Function(StartNavigation value)? startNavigation,
    TResult? Function(StopNavigation value)? stopNavigation,
    TResult? Function(SelectShop value)? selectRobot,
    TResult? Function(UpdateCurrentLocation value)? updateCurrentLocation,
  }) {
    return getCurrentLocation?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(PageOpened value)? pageOpened,
    TResult Function(GetCurrentLocation value)? getCurrentLocation,
    TResult Function(SearchByPostNumber value)? searchByPostNumber,
    TResult Function(OnPostNumberChange value)? onPostNumberChange,
    TResult Function(StartNavigation value)? startNavigation,
    TResult Function(StopNavigation value)? stopNavigation,
    TResult Function(SelectShop value)? selectRobot,
    TResult Function(UpdateCurrentLocation value)? updateCurrentLocation,
    required TResult orElse(),
  }) {
    if (getCurrentLocation != null) {
      return getCurrentLocation(this);
    }
    return orElse();
  }
}

abstract class GetCurrentLocation implements MapBlocEvent {
  factory GetCurrentLocation() = _$GetCurrentLocationImpl;
}

/// @nodoc
abstract class _$$SearchByPostNumberImplCopyWith<$Res> {
  factory _$$SearchByPostNumberImplCopyWith(_$SearchByPostNumberImpl value,
          $Res Function(_$SearchByPostNumberImpl) then) =
      __$$SearchByPostNumberImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$SearchByPostNumberImplCopyWithImpl<$Res>
    extends _$MapBlocEventCopyWithImpl<$Res, _$SearchByPostNumberImpl>
    implements _$$SearchByPostNumberImplCopyWith<$Res> {
  __$$SearchByPostNumberImplCopyWithImpl(_$SearchByPostNumberImpl _value,
      $Res Function(_$SearchByPostNumberImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$SearchByPostNumberImpl implements SearchByPostNumber {
  _$SearchByPostNumberImpl();

  @override
  String toString() {
    return 'MapBlocEvent.searchByPostNumber()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$SearchByPostNumberImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() pageOpened,
    required TResult Function() getCurrentLocation,
    required TResult Function() searchByPostNumber,
    required TResult Function(String value) onPostNumberChange,
    required TResult Function() startNavigation,
    required TResult Function() stopNavigation,
    required TResult Function(ShopEntity? shop) selectRobot,
    required TResult Function(LatLng location) updateCurrentLocation,
  }) {
    return searchByPostNumber();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? pageOpened,
    TResult? Function()? getCurrentLocation,
    TResult? Function()? searchByPostNumber,
    TResult? Function(String value)? onPostNumberChange,
    TResult? Function()? startNavigation,
    TResult? Function()? stopNavigation,
    TResult? Function(ShopEntity? shop)? selectRobot,
    TResult? Function(LatLng location)? updateCurrentLocation,
  }) {
    return searchByPostNumber?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? pageOpened,
    TResult Function()? getCurrentLocation,
    TResult Function()? searchByPostNumber,
    TResult Function(String value)? onPostNumberChange,
    TResult Function()? startNavigation,
    TResult Function()? stopNavigation,
    TResult Function(ShopEntity? shop)? selectRobot,
    TResult Function(LatLng location)? updateCurrentLocation,
    required TResult orElse(),
  }) {
    if (searchByPostNumber != null) {
      return searchByPostNumber();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(PageOpened value) pageOpened,
    required TResult Function(GetCurrentLocation value) getCurrentLocation,
    required TResult Function(SearchByPostNumber value) searchByPostNumber,
    required TResult Function(OnPostNumberChange value) onPostNumberChange,
    required TResult Function(StartNavigation value) startNavigation,
    required TResult Function(StopNavigation value) stopNavigation,
    required TResult Function(SelectShop value) selectRobot,
    required TResult Function(UpdateCurrentLocation value)
        updateCurrentLocation,
  }) {
    return searchByPostNumber(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(PageOpened value)? pageOpened,
    TResult? Function(GetCurrentLocation value)? getCurrentLocation,
    TResult? Function(SearchByPostNumber value)? searchByPostNumber,
    TResult? Function(OnPostNumberChange value)? onPostNumberChange,
    TResult? Function(StartNavigation value)? startNavigation,
    TResult? Function(StopNavigation value)? stopNavigation,
    TResult? Function(SelectShop value)? selectRobot,
    TResult? Function(UpdateCurrentLocation value)? updateCurrentLocation,
  }) {
    return searchByPostNumber?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(PageOpened value)? pageOpened,
    TResult Function(GetCurrentLocation value)? getCurrentLocation,
    TResult Function(SearchByPostNumber value)? searchByPostNumber,
    TResult Function(OnPostNumberChange value)? onPostNumberChange,
    TResult Function(StartNavigation value)? startNavigation,
    TResult Function(StopNavigation value)? stopNavigation,
    TResult Function(SelectShop value)? selectRobot,
    TResult Function(UpdateCurrentLocation value)? updateCurrentLocation,
    required TResult orElse(),
  }) {
    if (searchByPostNumber != null) {
      return searchByPostNumber(this);
    }
    return orElse();
  }
}

abstract class SearchByPostNumber implements MapBlocEvent {
  factory SearchByPostNumber() = _$SearchByPostNumberImpl;
}

/// @nodoc
abstract class _$$OnPostNumberChangeImplCopyWith<$Res> {
  factory _$$OnPostNumberChangeImplCopyWith(_$OnPostNumberChangeImpl value,
          $Res Function(_$OnPostNumberChangeImpl) then) =
      __$$OnPostNumberChangeImplCopyWithImpl<$Res>;
  @useResult
  $Res call({String value});
}

/// @nodoc
class __$$OnPostNumberChangeImplCopyWithImpl<$Res>
    extends _$MapBlocEventCopyWithImpl<$Res, _$OnPostNumberChangeImpl>
    implements _$$OnPostNumberChangeImplCopyWith<$Res> {
  __$$OnPostNumberChangeImplCopyWithImpl(_$OnPostNumberChangeImpl _value,
      $Res Function(_$OnPostNumberChangeImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? value = null,
  }) {
    return _then(_$OnPostNumberChangeImpl(
      null == value
          ? _value.value
          : value // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$OnPostNumberChangeImpl implements OnPostNumberChange {
  _$OnPostNumberChangeImpl(this.value);

  @override
  final String value;

  @override
  String toString() {
    return 'MapBlocEvent.onPostNumberChange(value: $value)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$OnPostNumberChangeImpl &&
            (identical(other.value, value) || other.value == value));
  }

  @override
  int get hashCode => Object.hash(runtimeType, value);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$OnPostNumberChangeImplCopyWith<_$OnPostNumberChangeImpl> get copyWith =>
      __$$OnPostNumberChangeImplCopyWithImpl<_$OnPostNumberChangeImpl>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() pageOpened,
    required TResult Function() getCurrentLocation,
    required TResult Function() searchByPostNumber,
    required TResult Function(String value) onPostNumberChange,
    required TResult Function() startNavigation,
    required TResult Function() stopNavigation,
    required TResult Function(ShopEntity? shop) selectRobot,
    required TResult Function(LatLng location) updateCurrentLocation,
  }) {
    return onPostNumberChange(value);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? pageOpened,
    TResult? Function()? getCurrentLocation,
    TResult? Function()? searchByPostNumber,
    TResult? Function(String value)? onPostNumberChange,
    TResult? Function()? startNavigation,
    TResult? Function()? stopNavigation,
    TResult? Function(ShopEntity? shop)? selectRobot,
    TResult? Function(LatLng location)? updateCurrentLocation,
  }) {
    return onPostNumberChange?.call(value);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? pageOpened,
    TResult Function()? getCurrentLocation,
    TResult Function()? searchByPostNumber,
    TResult Function(String value)? onPostNumberChange,
    TResult Function()? startNavigation,
    TResult Function()? stopNavigation,
    TResult Function(ShopEntity? shop)? selectRobot,
    TResult Function(LatLng location)? updateCurrentLocation,
    required TResult orElse(),
  }) {
    if (onPostNumberChange != null) {
      return onPostNumberChange(value);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(PageOpened value) pageOpened,
    required TResult Function(GetCurrentLocation value) getCurrentLocation,
    required TResult Function(SearchByPostNumber value) searchByPostNumber,
    required TResult Function(OnPostNumberChange value) onPostNumberChange,
    required TResult Function(StartNavigation value) startNavigation,
    required TResult Function(StopNavigation value) stopNavigation,
    required TResult Function(SelectShop value) selectRobot,
    required TResult Function(UpdateCurrentLocation value)
        updateCurrentLocation,
  }) {
    return onPostNumberChange(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(PageOpened value)? pageOpened,
    TResult? Function(GetCurrentLocation value)? getCurrentLocation,
    TResult? Function(SearchByPostNumber value)? searchByPostNumber,
    TResult? Function(OnPostNumberChange value)? onPostNumberChange,
    TResult? Function(StartNavigation value)? startNavigation,
    TResult? Function(StopNavigation value)? stopNavigation,
    TResult? Function(SelectShop value)? selectRobot,
    TResult? Function(UpdateCurrentLocation value)? updateCurrentLocation,
  }) {
    return onPostNumberChange?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(PageOpened value)? pageOpened,
    TResult Function(GetCurrentLocation value)? getCurrentLocation,
    TResult Function(SearchByPostNumber value)? searchByPostNumber,
    TResult Function(OnPostNumberChange value)? onPostNumberChange,
    TResult Function(StartNavigation value)? startNavigation,
    TResult Function(StopNavigation value)? stopNavigation,
    TResult Function(SelectShop value)? selectRobot,
    TResult Function(UpdateCurrentLocation value)? updateCurrentLocation,
    required TResult orElse(),
  }) {
    if (onPostNumberChange != null) {
      return onPostNumberChange(this);
    }
    return orElse();
  }
}

abstract class OnPostNumberChange implements MapBlocEvent {
  factory OnPostNumberChange(final String value) = _$OnPostNumberChangeImpl;

  String get value;
  @JsonKey(ignore: true)
  _$$OnPostNumberChangeImplCopyWith<_$OnPostNumberChangeImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$StartNavigationImplCopyWith<$Res> {
  factory _$$StartNavigationImplCopyWith(_$StartNavigationImpl value,
          $Res Function(_$StartNavigationImpl) then) =
      __$$StartNavigationImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$StartNavigationImplCopyWithImpl<$Res>
    extends _$MapBlocEventCopyWithImpl<$Res, _$StartNavigationImpl>
    implements _$$StartNavigationImplCopyWith<$Res> {
  __$$StartNavigationImplCopyWithImpl(
      _$StartNavigationImpl _value, $Res Function(_$StartNavigationImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$StartNavigationImpl implements StartNavigation {
  _$StartNavigationImpl();

  @override
  String toString() {
    return 'MapBlocEvent.startNavigation()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$StartNavigationImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() pageOpened,
    required TResult Function() getCurrentLocation,
    required TResult Function() searchByPostNumber,
    required TResult Function(String value) onPostNumberChange,
    required TResult Function() startNavigation,
    required TResult Function() stopNavigation,
    required TResult Function(ShopEntity? shop) selectRobot,
    required TResult Function(LatLng location) updateCurrentLocation,
  }) {
    return startNavigation();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? pageOpened,
    TResult? Function()? getCurrentLocation,
    TResult? Function()? searchByPostNumber,
    TResult? Function(String value)? onPostNumberChange,
    TResult? Function()? startNavigation,
    TResult? Function()? stopNavigation,
    TResult? Function(ShopEntity? shop)? selectRobot,
    TResult? Function(LatLng location)? updateCurrentLocation,
  }) {
    return startNavigation?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? pageOpened,
    TResult Function()? getCurrentLocation,
    TResult Function()? searchByPostNumber,
    TResult Function(String value)? onPostNumberChange,
    TResult Function()? startNavigation,
    TResult Function()? stopNavigation,
    TResult Function(ShopEntity? shop)? selectRobot,
    TResult Function(LatLng location)? updateCurrentLocation,
    required TResult orElse(),
  }) {
    if (startNavigation != null) {
      return startNavigation();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(PageOpened value) pageOpened,
    required TResult Function(GetCurrentLocation value) getCurrentLocation,
    required TResult Function(SearchByPostNumber value) searchByPostNumber,
    required TResult Function(OnPostNumberChange value) onPostNumberChange,
    required TResult Function(StartNavigation value) startNavigation,
    required TResult Function(StopNavigation value) stopNavigation,
    required TResult Function(SelectShop value) selectRobot,
    required TResult Function(UpdateCurrentLocation value)
        updateCurrentLocation,
  }) {
    return startNavigation(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(PageOpened value)? pageOpened,
    TResult? Function(GetCurrentLocation value)? getCurrentLocation,
    TResult? Function(SearchByPostNumber value)? searchByPostNumber,
    TResult? Function(OnPostNumberChange value)? onPostNumberChange,
    TResult? Function(StartNavigation value)? startNavigation,
    TResult? Function(StopNavigation value)? stopNavigation,
    TResult? Function(SelectShop value)? selectRobot,
    TResult? Function(UpdateCurrentLocation value)? updateCurrentLocation,
  }) {
    return startNavigation?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(PageOpened value)? pageOpened,
    TResult Function(GetCurrentLocation value)? getCurrentLocation,
    TResult Function(SearchByPostNumber value)? searchByPostNumber,
    TResult Function(OnPostNumberChange value)? onPostNumberChange,
    TResult Function(StartNavigation value)? startNavigation,
    TResult Function(StopNavigation value)? stopNavigation,
    TResult Function(SelectShop value)? selectRobot,
    TResult Function(UpdateCurrentLocation value)? updateCurrentLocation,
    required TResult orElse(),
  }) {
    if (startNavigation != null) {
      return startNavigation(this);
    }
    return orElse();
  }
}

abstract class StartNavigation implements MapBlocEvent {
  factory StartNavigation() = _$StartNavigationImpl;
}

/// @nodoc
abstract class _$$StopNavigationImplCopyWith<$Res> {
  factory _$$StopNavigationImplCopyWith(_$StopNavigationImpl value,
          $Res Function(_$StopNavigationImpl) then) =
      __$$StopNavigationImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$StopNavigationImplCopyWithImpl<$Res>
    extends _$MapBlocEventCopyWithImpl<$Res, _$StopNavigationImpl>
    implements _$$StopNavigationImplCopyWith<$Res> {
  __$$StopNavigationImplCopyWithImpl(
      _$StopNavigationImpl _value, $Res Function(_$StopNavigationImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$StopNavigationImpl implements StopNavigation {
  _$StopNavigationImpl();

  @override
  String toString() {
    return 'MapBlocEvent.stopNavigation()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$StopNavigationImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() pageOpened,
    required TResult Function() getCurrentLocation,
    required TResult Function() searchByPostNumber,
    required TResult Function(String value) onPostNumberChange,
    required TResult Function() startNavigation,
    required TResult Function() stopNavigation,
    required TResult Function(ShopEntity? shop) selectRobot,
    required TResult Function(LatLng location) updateCurrentLocation,
  }) {
    return stopNavigation();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? pageOpened,
    TResult? Function()? getCurrentLocation,
    TResult? Function()? searchByPostNumber,
    TResult? Function(String value)? onPostNumberChange,
    TResult? Function()? startNavigation,
    TResult? Function()? stopNavigation,
    TResult? Function(ShopEntity? shop)? selectRobot,
    TResult? Function(LatLng location)? updateCurrentLocation,
  }) {
    return stopNavigation?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? pageOpened,
    TResult Function()? getCurrentLocation,
    TResult Function()? searchByPostNumber,
    TResult Function(String value)? onPostNumberChange,
    TResult Function()? startNavigation,
    TResult Function()? stopNavigation,
    TResult Function(ShopEntity? shop)? selectRobot,
    TResult Function(LatLng location)? updateCurrentLocation,
    required TResult orElse(),
  }) {
    if (stopNavigation != null) {
      return stopNavigation();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(PageOpened value) pageOpened,
    required TResult Function(GetCurrentLocation value) getCurrentLocation,
    required TResult Function(SearchByPostNumber value) searchByPostNumber,
    required TResult Function(OnPostNumberChange value) onPostNumberChange,
    required TResult Function(StartNavigation value) startNavigation,
    required TResult Function(StopNavigation value) stopNavigation,
    required TResult Function(SelectShop value) selectRobot,
    required TResult Function(UpdateCurrentLocation value)
        updateCurrentLocation,
  }) {
    return stopNavigation(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(PageOpened value)? pageOpened,
    TResult? Function(GetCurrentLocation value)? getCurrentLocation,
    TResult? Function(SearchByPostNumber value)? searchByPostNumber,
    TResult? Function(OnPostNumberChange value)? onPostNumberChange,
    TResult? Function(StartNavigation value)? startNavigation,
    TResult? Function(StopNavigation value)? stopNavigation,
    TResult? Function(SelectShop value)? selectRobot,
    TResult? Function(UpdateCurrentLocation value)? updateCurrentLocation,
  }) {
    return stopNavigation?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(PageOpened value)? pageOpened,
    TResult Function(GetCurrentLocation value)? getCurrentLocation,
    TResult Function(SearchByPostNumber value)? searchByPostNumber,
    TResult Function(OnPostNumberChange value)? onPostNumberChange,
    TResult Function(StartNavigation value)? startNavigation,
    TResult Function(StopNavigation value)? stopNavigation,
    TResult Function(SelectShop value)? selectRobot,
    TResult Function(UpdateCurrentLocation value)? updateCurrentLocation,
    required TResult orElse(),
  }) {
    if (stopNavigation != null) {
      return stopNavigation(this);
    }
    return orElse();
  }
}

abstract class StopNavigation implements MapBlocEvent {
  factory StopNavigation() = _$StopNavigationImpl;
}

/// @nodoc
abstract class _$$SelectShopImplCopyWith<$Res> {
  factory _$$SelectShopImplCopyWith(
          _$SelectShopImpl value, $Res Function(_$SelectShopImpl) then) =
      __$$SelectShopImplCopyWithImpl<$Res>;
  @useResult
  $Res call({ShopEntity? shop});

  $ShopEntityCopyWith<$Res>? get shop;
}

/// @nodoc
class __$$SelectShopImplCopyWithImpl<$Res>
    extends _$MapBlocEventCopyWithImpl<$Res, _$SelectShopImpl>
    implements _$$SelectShopImplCopyWith<$Res> {
  __$$SelectShopImplCopyWithImpl(
      _$SelectShopImpl _value, $Res Function(_$SelectShopImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? shop = freezed,
  }) {
    return _then(_$SelectShopImpl(
      shop: freezed == shop
          ? _value.shop
          : shop // ignore: cast_nullable_to_non_nullable
              as ShopEntity?,
    ));
  }

  @override
  @pragma('vm:prefer-inline')
  $ShopEntityCopyWith<$Res>? get shop {
    if (_value.shop == null) {
      return null;
    }

    return $ShopEntityCopyWith<$Res>(_value.shop!, (value) {
      return _then(_value.copyWith(shop: value));
    });
  }
}

/// @nodoc

class _$SelectShopImpl implements SelectShop {
  _$SelectShopImpl({this.shop});

  @override
  final ShopEntity? shop;

  @override
  String toString() {
    return 'MapBlocEvent.selectRobot(shop: $shop)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$SelectShopImpl &&
            (identical(other.shop, shop) || other.shop == shop));
  }

  @override
  int get hashCode => Object.hash(runtimeType, shop);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$SelectShopImplCopyWith<_$SelectShopImpl> get copyWith =>
      __$$SelectShopImplCopyWithImpl<_$SelectShopImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() pageOpened,
    required TResult Function() getCurrentLocation,
    required TResult Function() searchByPostNumber,
    required TResult Function(String value) onPostNumberChange,
    required TResult Function() startNavigation,
    required TResult Function() stopNavigation,
    required TResult Function(ShopEntity? shop) selectRobot,
    required TResult Function(LatLng location) updateCurrentLocation,
  }) {
    return selectRobot(shop);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? pageOpened,
    TResult? Function()? getCurrentLocation,
    TResult? Function()? searchByPostNumber,
    TResult? Function(String value)? onPostNumberChange,
    TResult? Function()? startNavigation,
    TResult? Function()? stopNavigation,
    TResult? Function(ShopEntity? shop)? selectRobot,
    TResult? Function(LatLng location)? updateCurrentLocation,
  }) {
    return selectRobot?.call(shop);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? pageOpened,
    TResult Function()? getCurrentLocation,
    TResult Function()? searchByPostNumber,
    TResult Function(String value)? onPostNumberChange,
    TResult Function()? startNavigation,
    TResult Function()? stopNavigation,
    TResult Function(ShopEntity? shop)? selectRobot,
    TResult Function(LatLng location)? updateCurrentLocation,
    required TResult orElse(),
  }) {
    if (selectRobot != null) {
      return selectRobot(shop);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(PageOpened value) pageOpened,
    required TResult Function(GetCurrentLocation value) getCurrentLocation,
    required TResult Function(SearchByPostNumber value) searchByPostNumber,
    required TResult Function(OnPostNumberChange value) onPostNumberChange,
    required TResult Function(StartNavigation value) startNavigation,
    required TResult Function(StopNavigation value) stopNavigation,
    required TResult Function(SelectShop value) selectRobot,
    required TResult Function(UpdateCurrentLocation value)
        updateCurrentLocation,
  }) {
    return selectRobot(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(PageOpened value)? pageOpened,
    TResult? Function(GetCurrentLocation value)? getCurrentLocation,
    TResult? Function(SearchByPostNumber value)? searchByPostNumber,
    TResult? Function(OnPostNumberChange value)? onPostNumberChange,
    TResult? Function(StartNavigation value)? startNavigation,
    TResult? Function(StopNavigation value)? stopNavigation,
    TResult? Function(SelectShop value)? selectRobot,
    TResult? Function(UpdateCurrentLocation value)? updateCurrentLocation,
  }) {
    return selectRobot?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(PageOpened value)? pageOpened,
    TResult Function(GetCurrentLocation value)? getCurrentLocation,
    TResult Function(SearchByPostNumber value)? searchByPostNumber,
    TResult Function(OnPostNumberChange value)? onPostNumberChange,
    TResult Function(StartNavigation value)? startNavigation,
    TResult Function(StopNavigation value)? stopNavigation,
    TResult Function(SelectShop value)? selectRobot,
    TResult Function(UpdateCurrentLocation value)? updateCurrentLocation,
    required TResult orElse(),
  }) {
    if (selectRobot != null) {
      return selectRobot(this);
    }
    return orElse();
  }
}

abstract class SelectShop implements MapBlocEvent {
  factory SelectShop({final ShopEntity? shop}) = _$SelectShopImpl;

  ShopEntity? get shop;
  @JsonKey(ignore: true)
  _$$SelectShopImplCopyWith<_$SelectShopImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$UpdateCurrentLocationImplCopyWith<$Res> {
  factory _$$UpdateCurrentLocationImplCopyWith(
          _$UpdateCurrentLocationImpl value,
          $Res Function(_$UpdateCurrentLocationImpl) then) =
      __$$UpdateCurrentLocationImplCopyWithImpl<$Res>;
  @useResult
  $Res call({LatLng location});
}

/// @nodoc
class __$$UpdateCurrentLocationImplCopyWithImpl<$Res>
    extends _$MapBlocEventCopyWithImpl<$Res, _$UpdateCurrentLocationImpl>
    implements _$$UpdateCurrentLocationImplCopyWith<$Res> {
  __$$UpdateCurrentLocationImplCopyWithImpl(_$UpdateCurrentLocationImpl _value,
      $Res Function(_$UpdateCurrentLocationImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? location = null,
  }) {
    return _then(_$UpdateCurrentLocationImpl(
      null == location
          ? _value.location
          : location // ignore: cast_nullable_to_non_nullable
              as LatLng,
    ));
  }
}

/// @nodoc

class _$UpdateCurrentLocationImpl implements UpdateCurrentLocation {
  _$UpdateCurrentLocationImpl(this.location);

  @override
  final LatLng location;

  @override
  String toString() {
    return 'MapBlocEvent.updateCurrentLocation(location: $location)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$UpdateCurrentLocationImpl &&
            (identical(other.location, location) ||
                other.location == location));
  }

  @override
  int get hashCode => Object.hash(runtimeType, location);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$UpdateCurrentLocationImplCopyWith<_$UpdateCurrentLocationImpl>
      get copyWith => __$$UpdateCurrentLocationImplCopyWithImpl<
          _$UpdateCurrentLocationImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() pageOpened,
    required TResult Function() getCurrentLocation,
    required TResult Function() searchByPostNumber,
    required TResult Function(String value) onPostNumberChange,
    required TResult Function() startNavigation,
    required TResult Function() stopNavigation,
    required TResult Function(ShopEntity? shop) selectRobot,
    required TResult Function(LatLng location) updateCurrentLocation,
  }) {
    return updateCurrentLocation(location);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? pageOpened,
    TResult? Function()? getCurrentLocation,
    TResult? Function()? searchByPostNumber,
    TResult? Function(String value)? onPostNumberChange,
    TResult? Function()? startNavigation,
    TResult? Function()? stopNavigation,
    TResult? Function(ShopEntity? shop)? selectRobot,
    TResult? Function(LatLng location)? updateCurrentLocation,
  }) {
    return updateCurrentLocation?.call(location);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? pageOpened,
    TResult Function()? getCurrentLocation,
    TResult Function()? searchByPostNumber,
    TResult Function(String value)? onPostNumberChange,
    TResult Function()? startNavigation,
    TResult Function()? stopNavigation,
    TResult Function(ShopEntity? shop)? selectRobot,
    TResult Function(LatLng location)? updateCurrentLocation,
    required TResult orElse(),
  }) {
    if (updateCurrentLocation != null) {
      return updateCurrentLocation(location);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(PageOpened value) pageOpened,
    required TResult Function(GetCurrentLocation value) getCurrentLocation,
    required TResult Function(SearchByPostNumber value) searchByPostNumber,
    required TResult Function(OnPostNumberChange value) onPostNumberChange,
    required TResult Function(StartNavigation value) startNavigation,
    required TResult Function(StopNavigation value) stopNavigation,
    required TResult Function(SelectShop value) selectRobot,
    required TResult Function(UpdateCurrentLocation value)
        updateCurrentLocation,
  }) {
    return updateCurrentLocation(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(PageOpened value)? pageOpened,
    TResult? Function(GetCurrentLocation value)? getCurrentLocation,
    TResult? Function(SearchByPostNumber value)? searchByPostNumber,
    TResult? Function(OnPostNumberChange value)? onPostNumberChange,
    TResult? Function(StartNavigation value)? startNavigation,
    TResult? Function(StopNavigation value)? stopNavigation,
    TResult? Function(SelectShop value)? selectRobot,
    TResult? Function(UpdateCurrentLocation value)? updateCurrentLocation,
  }) {
    return updateCurrentLocation?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(PageOpened value)? pageOpened,
    TResult Function(GetCurrentLocation value)? getCurrentLocation,
    TResult Function(SearchByPostNumber value)? searchByPostNumber,
    TResult Function(OnPostNumberChange value)? onPostNumberChange,
    TResult Function(StartNavigation value)? startNavigation,
    TResult Function(StopNavigation value)? stopNavigation,
    TResult Function(SelectShop value)? selectRobot,
    TResult Function(UpdateCurrentLocation value)? updateCurrentLocation,
    required TResult orElse(),
  }) {
    if (updateCurrentLocation != null) {
      return updateCurrentLocation(this);
    }
    return orElse();
  }
}

abstract class UpdateCurrentLocation implements MapBlocEvent {
  factory UpdateCurrentLocation(final LatLng location) =
      _$UpdateCurrentLocationImpl;

  LatLng get location;
  @JsonKey(ignore: true)
  _$$UpdateCurrentLocationImplCopyWith<_$UpdateCurrentLocationImpl>
      get copyWith => throw _privateConstructorUsedError;
}
