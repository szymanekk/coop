import 'package:coop/models/RouteEntity.dart';
import 'package:coop/models/ShopEntity.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:latlong2/latlong.dart';
import 'package:coop/models/Failure.dart';
import 'package:coop/modules/base/BaseState.dart';

part 'MapBlocState.freezed.dart';

@freezed
class MapBlocState with _$MapBlocState implements BaseState {
  const MapBlocState._();

  @Implements<LoadingState>()
  const factory MapBlocState.loading() = LoadingPage;

  @Implements<ErrorState>()
  const factory MapBlocState.error({required Failure error}) = ErrorPage;

  @Implements<SuccessState>()
  const factory MapBlocState.success({
    required List<ShopEntity> shops,
    required LatLng currentLocation,
    required double currentZoom,
    required String search,
    required bool showPath,
    ShopEntity? selectedShop,
    RouteEntity? route,
  }) = SuccessPage;

  List<ShopEntity> get filteredShops => maybeMap(
      success: (success) => success.showPath && success.selectedShop != null
          ? [success.selectedShop!]
          : success.shops,
      orElse: () => []);
}
