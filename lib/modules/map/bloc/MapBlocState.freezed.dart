// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'MapBlocState.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$MapBlocState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loading,
    required TResult Function(Failure error) error,
    required TResult Function(
            List<ShopEntity> shops,
            LatLng currentLocation,
            double currentZoom,
            String search,
            bool showPath,
            ShopEntity? selectedShop,
            RouteEntity? route)
        success,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? loading,
    TResult? Function(Failure error)? error,
    TResult? Function(
            List<ShopEntity> shops,
            LatLng currentLocation,
            double currentZoom,
            String search,
            bool showPath,
            ShopEntity? selectedShop,
            RouteEntity? route)?
        success,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function(Failure error)? error,
    TResult Function(
            List<ShopEntity> shops,
            LatLng currentLocation,
            double currentZoom,
            String search,
            bool showPath,
            ShopEntity? selectedShop,
            RouteEntity? route)?
        success,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(LoadingPage value) loading,
    required TResult Function(ErrorPage value) error,
    required TResult Function(SuccessPage value) success,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(LoadingPage value)? loading,
    TResult? Function(ErrorPage value)? error,
    TResult? Function(SuccessPage value)? success,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(LoadingPage value)? loading,
    TResult Function(ErrorPage value)? error,
    TResult Function(SuccessPage value)? success,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $MapBlocStateCopyWith<$Res> {
  factory $MapBlocStateCopyWith(
          MapBlocState value, $Res Function(MapBlocState) then) =
      _$MapBlocStateCopyWithImpl<$Res, MapBlocState>;
}

/// @nodoc
class _$MapBlocStateCopyWithImpl<$Res, $Val extends MapBlocState>
    implements $MapBlocStateCopyWith<$Res> {
  _$MapBlocStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$LoadingPageImplCopyWith<$Res> {
  factory _$$LoadingPageImplCopyWith(
          _$LoadingPageImpl value, $Res Function(_$LoadingPageImpl) then) =
      __$$LoadingPageImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$LoadingPageImplCopyWithImpl<$Res>
    extends _$MapBlocStateCopyWithImpl<$Res, _$LoadingPageImpl>
    implements _$$LoadingPageImplCopyWith<$Res> {
  __$$LoadingPageImplCopyWithImpl(
      _$LoadingPageImpl _value, $Res Function(_$LoadingPageImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$LoadingPageImpl extends LoadingPage {
  const _$LoadingPageImpl() : super._();

  @override
  String toString() {
    return 'MapBlocState.loading()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$LoadingPageImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loading,
    required TResult Function(Failure error) error,
    required TResult Function(
            List<ShopEntity> shops,
            LatLng currentLocation,
            double currentZoom,
            String search,
            bool showPath,
            ShopEntity? selectedShop,
            RouteEntity? route)
        success,
  }) {
    return loading();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? loading,
    TResult? Function(Failure error)? error,
    TResult? Function(
            List<ShopEntity> shops,
            LatLng currentLocation,
            double currentZoom,
            String search,
            bool showPath,
            ShopEntity? selectedShop,
            RouteEntity? route)?
        success,
  }) {
    return loading?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function(Failure error)? error,
    TResult Function(
            List<ShopEntity> shops,
            LatLng currentLocation,
            double currentZoom,
            String search,
            bool showPath,
            ShopEntity? selectedShop,
            RouteEntity? route)?
        success,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(LoadingPage value) loading,
    required TResult Function(ErrorPage value) error,
    required TResult Function(SuccessPage value) success,
  }) {
    return loading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(LoadingPage value)? loading,
    TResult? Function(ErrorPage value)? error,
    TResult? Function(SuccessPage value)? success,
  }) {
    return loading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(LoadingPage value)? loading,
    TResult Function(ErrorPage value)? error,
    TResult Function(SuccessPage value)? success,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class LoadingPage extends MapBlocState implements LoadingState {
  const factory LoadingPage() = _$LoadingPageImpl;
  const LoadingPage._() : super._();
}

/// @nodoc
abstract class _$$ErrorPageImplCopyWith<$Res> {
  factory _$$ErrorPageImplCopyWith(
          _$ErrorPageImpl value, $Res Function(_$ErrorPageImpl) then) =
      __$$ErrorPageImplCopyWithImpl<$Res>;
  @useResult
  $Res call({Failure error});

  $FailureCopyWith<$Res> get error;
}

/// @nodoc
class __$$ErrorPageImplCopyWithImpl<$Res>
    extends _$MapBlocStateCopyWithImpl<$Res, _$ErrorPageImpl>
    implements _$$ErrorPageImplCopyWith<$Res> {
  __$$ErrorPageImplCopyWithImpl(
      _$ErrorPageImpl _value, $Res Function(_$ErrorPageImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? error = null,
  }) {
    return _then(_$ErrorPageImpl(
      error: null == error
          ? _value.error
          : error // ignore: cast_nullable_to_non_nullable
              as Failure,
    ));
  }

  @override
  @pragma('vm:prefer-inline')
  $FailureCopyWith<$Res> get error {
    return $FailureCopyWith<$Res>(_value.error, (value) {
      return _then(_value.copyWith(error: value));
    });
  }
}

/// @nodoc

class _$ErrorPageImpl extends ErrorPage {
  const _$ErrorPageImpl({required this.error}) : super._();

  @override
  final Failure error;

  @override
  String toString() {
    return 'MapBlocState.error(error: $error)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ErrorPageImpl &&
            (identical(other.error, error) || other.error == error));
  }

  @override
  int get hashCode => Object.hash(runtimeType, error);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ErrorPageImplCopyWith<_$ErrorPageImpl> get copyWith =>
      __$$ErrorPageImplCopyWithImpl<_$ErrorPageImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loading,
    required TResult Function(Failure error) error,
    required TResult Function(
            List<ShopEntity> shops,
            LatLng currentLocation,
            double currentZoom,
            String search,
            bool showPath,
            ShopEntity? selectedShop,
            RouteEntity? route)
        success,
  }) {
    return error(this.error);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? loading,
    TResult? Function(Failure error)? error,
    TResult? Function(
            List<ShopEntity> shops,
            LatLng currentLocation,
            double currentZoom,
            String search,
            bool showPath,
            ShopEntity? selectedShop,
            RouteEntity? route)?
        success,
  }) {
    return error?.call(this.error);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function(Failure error)? error,
    TResult Function(
            List<ShopEntity> shops,
            LatLng currentLocation,
            double currentZoom,
            String search,
            bool showPath,
            ShopEntity? selectedShop,
            RouteEntity? route)?
        success,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(this.error);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(LoadingPage value) loading,
    required TResult Function(ErrorPage value) error,
    required TResult Function(SuccessPage value) success,
  }) {
    return error(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(LoadingPage value)? loading,
    TResult? Function(ErrorPage value)? error,
    TResult? Function(SuccessPage value)? success,
  }) {
    return error?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(LoadingPage value)? loading,
    TResult Function(ErrorPage value)? error,
    TResult Function(SuccessPage value)? success,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(this);
    }
    return orElse();
  }
}

abstract class ErrorPage extends MapBlocState implements ErrorState {
  const factory ErrorPage({required final Failure error}) = _$ErrorPageImpl;
  const ErrorPage._() : super._();

  Failure get error;
  @JsonKey(ignore: true)
  _$$ErrorPageImplCopyWith<_$ErrorPageImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$SuccessPageImplCopyWith<$Res> {
  factory _$$SuccessPageImplCopyWith(
          _$SuccessPageImpl value, $Res Function(_$SuccessPageImpl) then) =
      __$$SuccessPageImplCopyWithImpl<$Res>;
  @useResult
  $Res call(
      {List<ShopEntity> shops,
      LatLng currentLocation,
      double currentZoom,
      String search,
      bool showPath,
      ShopEntity? selectedShop,
      RouteEntity? route});

  $ShopEntityCopyWith<$Res>? get selectedShop;
  $RouteEntityCopyWith<$Res>? get route;
}

/// @nodoc
class __$$SuccessPageImplCopyWithImpl<$Res>
    extends _$MapBlocStateCopyWithImpl<$Res, _$SuccessPageImpl>
    implements _$$SuccessPageImplCopyWith<$Res> {
  __$$SuccessPageImplCopyWithImpl(
      _$SuccessPageImpl _value, $Res Function(_$SuccessPageImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? shops = null,
    Object? currentLocation = null,
    Object? currentZoom = null,
    Object? search = null,
    Object? showPath = null,
    Object? selectedShop = freezed,
    Object? route = freezed,
  }) {
    return _then(_$SuccessPageImpl(
      shops: null == shops
          ? _value._shops
          : shops // ignore: cast_nullable_to_non_nullable
              as List<ShopEntity>,
      currentLocation: null == currentLocation
          ? _value.currentLocation
          : currentLocation // ignore: cast_nullable_to_non_nullable
              as LatLng,
      currentZoom: null == currentZoom
          ? _value.currentZoom
          : currentZoom // ignore: cast_nullable_to_non_nullable
              as double,
      search: null == search
          ? _value.search
          : search // ignore: cast_nullable_to_non_nullable
              as String,
      showPath: null == showPath
          ? _value.showPath
          : showPath // ignore: cast_nullable_to_non_nullable
              as bool,
      selectedShop: freezed == selectedShop
          ? _value.selectedShop
          : selectedShop // ignore: cast_nullable_to_non_nullable
              as ShopEntity?,
      route: freezed == route
          ? _value.route
          : route // ignore: cast_nullable_to_non_nullable
              as RouteEntity?,
    ));
  }

  @override
  @pragma('vm:prefer-inline')
  $ShopEntityCopyWith<$Res>? get selectedShop {
    if (_value.selectedShop == null) {
      return null;
    }

    return $ShopEntityCopyWith<$Res>(_value.selectedShop!, (value) {
      return _then(_value.copyWith(selectedShop: value));
    });
  }

  @override
  @pragma('vm:prefer-inline')
  $RouteEntityCopyWith<$Res>? get route {
    if (_value.route == null) {
      return null;
    }

    return $RouteEntityCopyWith<$Res>(_value.route!, (value) {
      return _then(_value.copyWith(route: value));
    });
  }
}

/// @nodoc

class _$SuccessPageImpl extends SuccessPage {
  const _$SuccessPageImpl(
      {required final List<ShopEntity> shops,
      required this.currentLocation,
      required this.currentZoom,
      required this.search,
      required this.showPath,
      this.selectedShop,
      this.route})
      : _shops = shops,
        super._();

  final List<ShopEntity> _shops;
  @override
  List<ShopEntity> get shops {
    if (_shops is EqualUnmodifiableListView) return _shops;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_shops);
  }

  @override
  final LatLng currentLocation;
  @override
  final double currentZoom;
  @override
  final String search;
  @override
  final bool showPath;
  @override
  final ShopEntity? selectedShop;
  @override
  final RouteEntity? route;

  @override
  String toString() {
    return 'MapBlocState.success(shops: $shops, currentLocation: $currentLocation, currentZoom: $currentZoom, search: $search, showPath: $showPath, selectedShop: $selectedShop, route: $route)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$SuccessPageImpl &&
            const DeepCollectionEquality().equals(other._shops, _shops) &&
            (identical(other.currentLocation, currentLocation) ||
                other.currentLocation == currentLocation) &&
            (identical(other.currentZoom, currentZoom) ||
                other.currentZoom == currentZoom) &&
            (identical(other.search, search) || other.search == search) &&
            (identical(other.showPath, showPath) ||
                other.showPath == showPath) &&
            (identical(other.selectedShop, selectedShop) ||
                other.selectedShop == selectedShop) &&
            (identical(other.route, route) || other.route == route));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(_shops),
      currentLocation,
      currentZoom,
      search,
      showPath,
      selectedShop,
      route);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$SuccessPageImplCopyWith<_$SuccessPageImpl> get copyWith =>
      __$$SuccessPageImplCopyWithImpl<_$SuccessPageImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loading,
    required TResult Function(Failure error) error,
    required TResult Function(
            List<ShopEntity> shops,
            LatLng currentLocation,
            double currentZoom,
            String search,
            bool showPath,
            ShopEntity? selectedShop,
            RouteEntity? route)
        success,
  }) {
    return success(shops, currentLocation, currentZoom, search, showPath,
        selectedShop, route);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? loading,
    TResult? Function(Failure error)? error,
    TResult? Function(
            List<ShopEntity> shops,
            LatLng currentLocation,
            double currentZoom,
            String search,
            bool showPath,
            ShopEntity? selectedShop,
            RouteEntity? route)?
        success,
  }) {
    return success?.call(shops, currentLocation, currentZoom, search, showPath,
        selectedShop, route);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function(Failure error)? error,
    TResult Function(
            List<ShopEntity> shops,
            LatLng currentLocation,
            double currentZoom,
            String search,
            bool showPath,
            ShopEntity? selectedShop,
            RouteEntity? route)?
        success,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(shops, currentLocation, currentZoom, search, showPath,
          selectedShop, route);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(LoadingPage value) loading,
    required TResult Function(ErrorPage value) error,
    required TResult Function(SuccessPage value) success,
  }) {
    return success(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(LoadingPage value)? loading,
    TResult? Function(ErrorPage value)? error,
    TResult? Function(SuccessPage value)? success,
  }) {
    return success?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(LoadingPage value)? loading,
    TResult Function(ErrorPage value)? error,
    TResult Function(SuccessPage value)? success,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(this);
    }
    return orElse();
  }
}

abstract class SuccessPage extends MapBlocState implements SuccessState {
  const factory SuccessPage(
      {required final List<ShopEntity> shops,
      required final LatLng currentLocation,
      required final double currentZoom,
      required final String search,
      required final bool showPath,
      final ShopEntity? selectedShop,
      final RouteEntity? route}) = _$SuccessPageImpl;
  const SuccessPage._() : super._();

  List<ShopEntity> get shops;
  LatLng get currentLocation;
  double get currentZoom;
  String get search;
  bool get showPath;
  ShopEntity? get selectedShop;
  RouteEntity? get route;
  @JsonKey(ignore: true)
  _$$SuccessPageImplCopyWith<_$SuccessPageImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
