import 'package:coop/Di.dart';
import 'package:coop/models/ShopEntity.dart';
import 'package:coop/modules/shop/bloc/ShopBloc.dart';
import 'package:coop/modules/shop/bloc/ShopEvent.dart';
import 'package:coop/modules/shop/bloc/ShopState.dart';
import 'package:coop/modules/base/BasePageWidget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:coop/resources/AppConstants.dart';

class ShopPage extends StatelessWidget {
  static const routeName = '/shop';

  const ShopPage({super.key});

  @override
  Widget build(BuildContext context) => BlocProvider<ShopBloc>(
        create: (BuildContext context) => ShopBloc(getIt()),
        child: const ShopPageWidget(),
      );
}

class ShopPageWidget extends StatefulWidget {
  const ShopPageWidget({super.key});

  @override
  ShopPageState createState() => ShopPageState();
}

class ShopPageState extends BasePageStateWidget<ShopBloc, ShopEvent, ShopState,
    SuccessPage, ErrorPage, ShopEntity> {
  @override
  void onPageOpened() => context.read<ShopBloc>().add(PageOpened(argument));

  List<Widget> objectList(SuccessPage state) => [
        openNowCard(state),
        if (state.shop.address != null) address(state),
        if (state.shop.phone != null) phone(state),
        if (state.shop.email != null) email(state),
        if (state.shop.openingHours.isNotEmpty) openingHours(state.shop),
      ];

  Widget openNowCard(SuccessPage state) => Card(
        color: state.shop.isOpenNow
            ? theme.colorScheme.secondary
            : theme.colorScheme.background,
        child: Padding(
          padding: const EdgeInsets.all(UiConstant.cardPadding),
          child: Center(
            child: Text(
              state.shop.isOpenNow ? strings.openNow : strings.closedNow,
              style: theme.textTheme.labelMedium?.copyWith(
                  color: state.shop.isOpenNow
                      ? theme.colorScheme.background
                      : theme.colorScheme.onBackground),
            ),
          ),
        ),
      );

  Widget info(IconData icon, String value) => Card(
        child: Column(
          children: [
            ListTile(
              leading: Icon(icon),
              title: Text(value),
            ),
          ],
        ),
      );

  Widget openingHours(ShopEntity shop) => Card(
    child: Padding(
      padding: const EdgeInsets.all(UiConstant.cardPadding),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: shop.openingHours.map((e) => Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
          Text(e.date), Text(e.day), Text(e.openHours),
        ],)).toList()
      ),
    ),
  );

  Widget address(SuccessPage state) =>
      info(Icons.pin_drop_rounded, state.shop.address!);

  Widget phone(SuccessPage state) => info(Icons.phone, state.shop.phone!);

  Widget email(SuccessPage state) => info(Icons.email, state.shop.email!);

  @override
  Widget successContent(SuccessPage state) => SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Expanded(
              child: CustomScrollView(
                slivers: <Widget>[
                  SliverList(
                    delegate: SliverChildBuilderDelegate(
                      (BuildContext context, int index) =>
                          objectList(state).elementAt(index),
                      childCount: objectList(state)
                          .length, // Number of children in the list
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      );

  @override
  PreferredSizeWidget? get appBar => AppBar(
        title: Text(argument.name),
      );

  @override
  Widget? get floatingButtonWidget => FloatingActionButton(
        onPressed: () => context.read<ShopBloc>().add(DownloadNewsletter()),
        child: const Icon(Icons.notifications),
      );
}
