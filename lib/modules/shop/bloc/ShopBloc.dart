import 'package:coop/models/Failure.dart';
import 'package:coop/repositories/ShopRepositoryBase.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:url_launcher/url_launcher.dart';

import 'package:coop/modules/base/BaseBloc.dart';
import 'package:coop/modules/base/BaseState.dart';
import 'ShopEvent.dart';
import 'ShopState.dart';

class ShopBloc extends BaseBloc<ShopEvent, ShopState> {
  ShopBloc(this.shopRepository) : super(const LoadingPage()) {
    on<PageOpened>(onPageOpened);
    on<DownloadNewsletter>(downloadNewsletter);
  }

  final ShopRepositoryBase shopRepository;

  void downloadNewsletter(
    DownloadNewsletter event,
    Emitter<ShopState> emit,
  ) async =>
      handleEvent(emitter: emit, onFunction: (currentState) async {
        final current = currentState as SuccessPage;
        final uri = Uri.parse(current.shop.newspaperUrl);
        await launchUrl(uri);
      });

  Future<void> onPageOpened(
    PageOpened event,
    Emitter<ShopState> emit,
  ) async {
    emit(SuccessPage(shop: event.shop));
  }

  @override
  void onCatchError(Emitter<BaseState> emitter, Failure failure) {
    emitter(ErrorPage(error: failure));
  }
}
