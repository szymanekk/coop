import 'package:freezed_annotation/freezed_annotation.dart';

import 'package:coop/models/ShopEntity.dart';
import 'package:coop/modules/base/BaseEvent.dart';

part 'ShopEvent.freezed.dart';

@freezed
class ShopEvent with _$ShopEvent implements BaseEvent{
  factory ShopEvent.pageOpened(ShopEntity shop) = PageOpened;
  factory ShopEvent.downloadNewsletter() = DownloadNewsletter;
}
