// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'ShopEvent.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$ShopEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(ShopEntity shop) pageOpened,
    required TResult Function() downloadNewsletter,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(ShopEntity shop)? pageOpened,
    TResult? Function()? downloadNewsletter,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(ShopEntity shop)? pageOpened,
    TResult Function()? downloadNewsletter,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(PageOpened value) pageOpened,
    required TResult Function(DownloadNewsletter value) downloadNewsletter,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(PageOpened value)? pageOpened,
    TResult? Function(DownloadNewsletter value)? downloadNewsletter,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(PageOpened value)? pageOpened,
    TResult Function(DownloadNewsletter value)? downloadNewsletter,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ShopEventCopyWith<$Res> {
  factory $ShopEventCopyWith(ShopEvent value, $Res Function(ShopEvent) then) =
      _$ShopEventCopyWithImpl<$Res, ShopEvent>;
}

/// @nodoc
class _$ShopEventCopyWithImpl<$Res, $Val extends ShopEvent>
    implements $ShopEventCopyWith<$Res> {
  _$ShopEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$PageOpenedImplCopyWith<$Res> {
  factory _$$PageOpenedImplCopyWith(
          _$PageOpenedImpl value, $Res Function(_$PageOpenedImpl) then) =
      __$$PageOpenedImplCopyWithImpl<$Res>;
  @useResult
  $Res call({ShopEntity shop});

  $ShopEntityCopyWith<$Res> get shop;
}

/// @nodoc
class __$$PageOpenedImplCopyWithImpl<$Res>
    extends _$ShopEventCopyWithImpl<$Res, _$PageOpenedImpl>
    implements _$$PageOpenedImplCopyWith<$Res> {
  __$$PageOpenedImplCopyWithImpl(
      _$PageOpenedImpl _value, $Res Function(_$PageOpenedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? shop = null,
  }) {
    return _then(_$PageOpenedImpl(
      null == shop
          ? _value.shop
          : shop // ignore: cast_nullable_to_non_nullable
              as ShopEntity,
    ));
  }

  @override
  @pragma('vm:prefer-inline')
  $ShopEntityCopyWith<$Res> get shop {
    return $ShopEntityCopyWith<$Res>(_value.shop, (value) {
      return _then(_value.copyWith(shop: value));
    });
  }
}

/// @nodoc

class _$PageOpenedImpl implements PageOpened {
  _$PageOpenedImpl(this.shop);

  @override
  final ShopEntity shop;

  @override
  String toString() {
    return 'ShopEvent.pageOpened(shop: $shop)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$PageOpenedImpl &&
            (identical(other.shop, shop) || other.shop == shop));
  }

  @override
  int get hashCode => Object.hash(runtimeType, shop);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$PageOpenedImplCopyWith<_$PageOpenedImpl> get copyWith =>
      __$$PageOpenedImplCopyWithImpl<_$PageOpenedImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(ShopEntity shop) pageOpened,
    required TResult Function() downloadNewsletter,
  }) {
    return pageOpened(shop);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(ShopEntity shop)? pageOpened,
    TResult? Function()? downloadNewsletter,
  }) {
    return pageOpened?.call(shop);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(ShopEntity shop)? pageOpened,
    TResult Function()? downloadNewsletter,
    required TResult orElse(),
  }) {
    if (pageOpened != null) {
      return pageOpened(shop);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(PageOpened value) pageOpened,
    required TResult Function(DownloadNewsletter value) downloadNewsletter,
  }) {
    return pageOpened(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(PageOpened value)? pageOpened,
    TResult? Function(DownloadNewsletter value)? downloadNewsletter,
  }) {
    return pageOpened?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(PageOpened value)? pageOpened,
    TResult Function(DownloadNewsletter value)? downloadNewsletter,
    required TResult orElse(),
  }) {
    if (pageOpened != null) {
      return pageOpened(this);
    }
    return orElse();
  }
}

abstract class PageOpened implements ShopEvent {
  factory PageOpened(final ShopEntity shop) = _$PageOpenedImpl;

  ShopEntity get shop;
  @JsonKey(ignore: true)
  _$$PageOpenedImplCopyWith<_$PageOpenedImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$DownloadNewsletterImplCopyWith<$Res> {
  factory _$$DownloadNewsletterImplCopyWith(_$DownloadNewsletterImpl value,
          $Res Function(_$DownloadNewsletterImpl) then) =
      __$$DownloadNewsletterImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$DownloadNewsletterImplCopyWithImpl<$Res>
    extends _$ShopEventCopyWithImpl<$Res, _$DownloadNewsletterImpl>
    implements _$$DownloadNewsletterImplCopyWith<$Res> {
  __$$DownloadNewsletterImplCopyWithImpl(_$DownloadNewsletterImpl _value,
      $Res Function(_$DownloadNewsletterImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$DownloadNewsletterImpl implements DownloadNewsletter {
  _$DownloadNewsletterImpl();

  @override
  String toString() {
    return 'ShopEvent.downloadNewsletter()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$DownloadNewsletterImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(ShopEntity shop) pageOpened,
    required TResult Function() downloadNewsletter,
  }) {
    return downloadNewsletter();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(ShopEntity shop)? pageOpened,
    TResult? Function()? downloadNewsletter,
  }) {
    return downloadNewsletter?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(ShopEntity shop)? pageOpened,
    TResult Function()? downloadNewsletter,
    required TResult orElse(),
  }) {
    if (downloadNewsletter != null) {
      return downloadNewsletter();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(PageOpened value) pageOpened,
    required TResult Function(DownloadNewsletter value) downloadNewsletter,
  }) {
    return downloadNewsletter(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(PageOpened value)? pageOpened,
    TResult? Function(DownloadNewsletter value)? downloadNewsletter,
  }) {
    return downloadNewsletter?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(PageOpened value)? pageOpened,
    TResult Function(DownloadNewsletter value)? downloadNewsletter,
    required TResult orElse(),
  }) {
    if (downloadNewsletter != null) {
      return downloadNewsletter(this);
    }
    return orElse();
  }
}

abstract class DownloadNewsletter implements ShopEvent {
  factory DownloadNewsletter() = _$DownloadNewsletterImpl;
}
