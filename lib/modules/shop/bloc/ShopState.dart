import 'package:coop/models/ShopEntity.dart';
import 'package:coop/modules/base/BaseState.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:coop/models/Failure.dart';

part 'ShopState.freezed.dart';

@freezed
class ShopState with _$ShopState implements BaseState{
  const ShopState._();

  @Implements<LoadingState>()
  const factory ShopState.loading() = LoadingPage;

  @Implements<ErrorState>()
  const factory ShopState.error({required Failure error}) = ErrorPage;

  @Implements<SuccessState>()
  const factory ShopState.success({
    required ShopEntity shop,
  }) = SuccessPage;
}
