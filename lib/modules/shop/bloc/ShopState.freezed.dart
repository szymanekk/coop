// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'ShopState.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$ShopState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loading,
    required TResult Function(Failure error) error,
    required TResult Function(ShopEntity shop) success,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? loading,
    TResult? Function(Failure error)? error,
    TResult? Function(ShopEntity shop)? success,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function(Failure error)? error,
    TResult Function(ShopEntity shop)? success,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(LoadingPage value) loading,
    required TResult Function(ErrorPage value) error,
    required TResult Function(SuccessPage value) success,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(LoadingPage value)? loading,
    TResult? Function(ErrorPage value)? error,
    TResult? Function(SuccessPage value)? success,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(LoadingPage value)? loading,
    TResult Function(ErrorPage value)? error,
    TResult Function(SuccessPage value)? success,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ShopStateCopyWith<$Res> {
  factory $ShopStateCopyWith(ShopState value, $Res Function(ShopState) then) =
      _$ShopStateCopyWithImpl<$Res, ShopState>;
}

/// @nodoc
class _$ShopStateCopyWithImpl<$Res, $Val extends ShopState>
    implements $ShopStateCopyWith<$Res> {
  _$ShopStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$LoadingPageImplCopyWith<$Res> {
  factory _$$LoadingPageImplCopyWith(
          _$LoadingPageImpl value, $Res Function(_$LoadingPageImpl) then) =
      __$$LoadingPageImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$LoadingPageImplCopyWithImpl<$Res>
    extends _$ShopStateCopyWithImpl<$Res, _$LoadingPageImpl>
    implements _$$LoadingPageImplCopyWith<$Res> {
  __$$LoadingPageImplCopyWithImpl(
      _$LoadingPageImpl _value, $Res Function(_$LoadingPageImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$LoadingPageImpl extends LoadingPage {
  const _$LoadingPageImpl() : super._();

  @override
  String toString() {
    return 'ShopState.loading()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$LoadingPageImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loading,
    required TResult Function(Failure error) error,
    required TResult Function(ShopEntity shop) success,
  }) {
    return loading();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? loading,
    TResult? Function(Failure error)? error,
    TResult? Function(ShopEntity shop)? success,
  }) {
    return loading?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function(Failure error)? error,
    TResult Function(ShopEntity shop)? success,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(LoadingPage value) loading,
    required TResult Function(ErrorPage value) error,
    required TResult Function(SuccessPage value) success,
  }) {
    return loading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(LoadingPage value)? loading,
    TResult? Function(ErrorPage value)? error,
    TResult? Function(SuccessPage value)? success,
  }) {
    return loading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(LoadingPage value)? loading,
    TResult Function(ErrorPage value)? error,
    TResult Function(SuccessPage value)? success,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class LoadingPage extends ShopState implements LoadingState {
  const factory LoadingPage() = _$LoadingPageImpl;
  const LoadingPage._() : super._();
}

/// @nodoc
abstract class _$$ErrorPageImplCopyWith<$Res> {
  factory _$$ErrorPageImplCopyWith(
          _$ErrorPageImpl value, $Res Function(_$ErrorPageImpl) then) =
      __$$ErrorPageImplCopyWithImpl<$Res>;
  @useResult
  $Res call({Failure error});

  $FailureCopyWith<$Res> get error;
}

/// @nodoc
class __$$ErrorPageImplCopyWithImpl<$Res>
    extends _$ShopStateCopyWithImpl<$Res, _$ErrorPageImpl>
    implements _$$ErrorPageImplCopyWith<$Res> {
  __$$ErrorPageImplCopyWithImpl(
      _$ErrorPageImpl _value, $Res Function(_$ErrorPageImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? error = null,
  }) {
    return _then(_$ErrorPageImpl(
      error: null == error
          ? _value.error
          : error // ignore: cast_nullable_to_non_nullable
              as Failure,
    ));
  }

  @override
  @pragma('vm:prefer-inline')
  $FailureCopyWith<$Res> get error {
    return $FailureCopyWith<$Res>(_value.error, (value) {
      return _then(_value.copyWith(error: value));
    });
  }
}

/// @nodoc

class _$ErrorPageImpl extends ErrorPage {
  const _$ErrorPageImpl({required this.error}) : super._();

  @override
  final Failure error;

  @override
  String toString() {
    return 'ShopState.error(error: $error)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ErrorPageImpl &&
            (identical(other.error, error) || other.error == error));
  }

  @override
  int get hashCode => Object.hash(runtimeType, error);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ErrorPageImplCopyWith<_$ErrorPageImpl> get copyWith =>
      __$$ErrorPageImplCopyWithImpl<_$ErrorPageImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loading,
    required TResult Function(Failure error) error,
    required TResult Function(ShopEntity shop) success,
  }) {
    return error(this.error);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? loading,
    TResult? Function(Failure error)? error,
    TResult? Function(ShopEntity shop)? success,
  }) {
    return error?.call(this.error);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function(Failure error)? error,
    TResult Function(ShopEntity shop)? success,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(this.error);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(LoadingPage value) loading,
    required TResult Function(ErrorPage value) error,
    required TResult Function(SuccessPage value) success,
  }) {
    return error(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(LoadingPage value)? loading,
    TResult? Function(ErrorPage value)? error,
    TResult? Function(SuccessPage value)? success,
  }) {
    return error?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(LoadingPage value)? loading,
    TResult Function(ErrorPage value)? error,
    TResult Function(SuccessPage value)? success,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(this);
    }
    return orElse();
  }
}

abstract class ErrorPage extends ShopState implements ErrorState {
  const factory ErrorPage({required final Failure error}) = _$ErrorPageImpl;
  const ErrorPage._() : super._();

  Failure get error;
  @JsonKey(ignore: true)
  _$$ErrorPageImplCopyWith<_$ErrorPageImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$SuccessPageImplCopyWith<$Res> {
  factory _$$SuccessPageImplCopyWith(
          _$SuccessPageImpl value, $Res Function(_$SuccessPageImpl) then) =
      __$$SuccessPageImplCopyWithImpl<$Res>;
  @useResult
  $Res call({ShopEntity shop});

  $ShopEntityCopyWith<$Res> get shop;
}

/// @nodoc
class __$$SuccessPageImplCopyWithImpl<$Res>
    extends _$ShopStateCopyWithImpl<$Res, _$SuccessPageImpl>
    implements _$$SuccessPageImplCopyWith<$Res> {
  __$$SuccessPageImplCopyWithImpl(
      _$SuccessPageImpl _value, $Res Function(_$SuccessPageImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? shop = null,
  }) {
    return _then(_$SuccessPageImpl(
      shop: null == shop
          ? _value.shop
          : shop // ignore: cast_nullable_to_non_nullable
              as ShopEntity,
    ));
  }

  @override
  @pragma('vm:prefer-inline')
  $ShopEntityCopyWith<$Res> get shop {
    return $ShopEntityCopyWith<$Res>(_value.shop, (value) {
      return _then(_value.copyWith(shop: value));
    });
  }
}

/// @nodoc

class _$SuccessPageImpl extends SuccessPage {
  const _$SuccessPageImpl({required this.shop}) : super._();

  @override
  final ShopEntity shop;

  @override
  String toString() {
    return 'ShopState.success(shop: $shop)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$SuccessPageImpl &&
            (identical(other.shop, shop) || other.shop == shop));
  }

  @override
  int get hashCode => Object.hash(runtimeType, shop);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$SuccessPageImplCopyWith<_$SuccessPageImpl> get copyWith =>
      __$$SuccessPageImplCopyWithImpl<_$SuccessPageImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loading,
    required TResult Function(Failure error) error,
    required TResult Function(ShopEntity shop) success,
  }) {
    return success(shop);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? loading,
    TResult? Function(Failure error)? error,
    TResult? Function(ShopEntity shop)? success,
  }) {
    return success?.call(shop);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function(Failure error)? error,
    TResult Function(ShopEntity shop)? success,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(shop);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(LoadingPage value) loading,
    required TResult Function(ErrorPage value) error,
    required TResult Function(SuccessPage value) success,
  }) {
    return success(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(LoadingPage value)? loading,
    TResult? Function(ErrorPage value)? error,
    TResult? Function(SuccessPage value)? success,
  }) {
    return success?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(LoadingPage value)? loading,
    TResult Function(ErrorPage value)? error,
    TResult Function(SuccessPage value)? success,
    required TResult orElse(),
  }) {
    if (success != null) {
      return success(this);
    }
    return orElse();
  }
}

abstract class SuccessPage extends ShopState implements SuccessState {
  const factory SuccessPage({required final ShopEntity shop}) =
      _$SuccessPageImpl;
  const SuccessPage._() : super._();

  ShopEntity get shop;
  @JsonKey(ignore: true)
  _$$SuccessPageImplCopyWith<_$SuccessPageImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
