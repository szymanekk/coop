import 'package:coop/models/RouteEntity.dart';
import 'package:coop/repositories/RouteRepositoryBase.dart';
import 'package:latlong2/latlong.dart';
import 'package:coop/services/routeServiceBase.dart';

class RouteRepository implements RouteRepositoryBase {
  final RouteServiceBase routeService;

  const RouteRepository(this.routeService);

  @override
  Future<RouteEntity> getRoute(
          {required LatLng currentLocation, required LatLng shopLocation}) =>
      routeService.getRoute(currentLocation, shopLocation);
}
