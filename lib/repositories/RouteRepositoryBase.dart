import 'package:latlong2/latlong.dart';
import 'package:coop/models/RouteEntity.dart';

abstract class RouteRepositoryBase {
  Future<RouteEntity> getRoute(
      {required LatLng currentLocation, required LatLng shopLocation});
}
