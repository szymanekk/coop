import 'package:coop/repositories/ShopRepositoryBase.dart';
import 'package:coop/services/ShopServiceBase.dart';
import 'package:coop/models/ShopEntity.dart';
import 'package:latlong2/latlong.dart';

class ShopRepository implements ShopRepositoryBase {
  final ShopServiceBase shopService;

  const ShopRepository(this.shopService);

  @override
  Future<List<ShopEntity>> getShopsByLocation({required LatLng location}) =>
      shopService.getShops(
          latitude: location.latitude.toString(),
          longitude: location.longitude.toString());

  @override
  Future<List<ShopEntity>> getShopsByPostNumber({required String postNumber}) =>
      shopService.getShops();
}
