import 'package:coop/models/ShopEntity.dart';
import 'package:latlong2/latlong.dart';

abstract class ShopRepositoryBase {
  Future<List<ShopEntity>> getShopsByLocation({required LatLng location});

  Future<List<ShopEntity>> getShopsByPostNumber({required String postNumber});
}
