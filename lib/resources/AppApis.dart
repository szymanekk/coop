class AppApis {
  static String mapToken =
      'pk.eyJ1Ijoic3p5bWFuZWtrbSIsImEiOiJjbHNxOGc4bHgweHdtMmpxcXA0eGIwcmtmIn0.Fv9TruuHljjWtGMHrl1_Fw';
  static String map =
      'https://api.mapbox.com/styles/v1/szymanekkm/clsq8ixcu003z01qt1qa5d182/tiles/256/{z}/{x}/{y}@2x?access_token=$mapToken';
  static String mapRoute =
      'https://api.mapbox.com/directions/v5/mapbox/driving';
  static String baseUrl = 'https://minside.coop.no';
  static String stores = '$baseUrl/StoreService';
}
