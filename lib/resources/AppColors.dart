import 'package:flutter/material.dart';

class AppColors {
  static const yellow = Color.fromRGBO(255,190,0, 1);
  static const blue = Color.fromRGBO(4,36,84, 1);
  static const background = Colors.white;
  static const gray = Color.fromRGBO(205, 208, 212, 1);
  static const disabled = Colors.grey;
}
