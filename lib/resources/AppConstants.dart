import 'package:latlong2/latlong.dart';

class ConstantLocations {
  static LatLng osloLocation = const LatLng(59.91273, 10.74609);
}

class ConstantResponseCodes {
  static const int success = 200;
  static const int noContent = 201;
  static const int badRequest = 400;
  static const int unauthorized = 401;
  static const int forbidden = 403;
  static const int internalServerError = 500;
  static const int notFound = 404;
}

class UiConstant {
  static const double baseTextSpace = 4.0;
  static const double cardPadding = 12.0;
  static const double defaultButtonPadding = 12.0;
  static const double defaultBorderRadius = 8.0;
  static const double defaultMarkerIcon = 12.0;
  static const double defaultMarkerPadding = 2.0;
}
