import 'package:coop/modules/SplashPage.dart';
import 'package:coop/modules/map/MapPage.dart';
import 'package:coop/modules/shop/ShopPage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class AppRoutes {
  static Map<String, WidgetBuilder> get routeBuilders =>
      <String, WidgetBuilder>{
        SplashPage.routeName: (_) => const SplashPage(),
        MapPage.routeName: (_) => const MapPage(),
        ShopPage.routeName: (_) => const ShopPage(),
      };
}
