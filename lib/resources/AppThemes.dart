import 'package:coop/resources/AppColors.dart';
import 'package:flutter/material.dart';

class AppThemes {
  static EdgeInsets get defaultButtonPadding =>
      const EdgeInsets.symmetric(horizontal: 8.0);

  static RoundedRectangleBorder get defaultButtonShape =>
      RoundedRectangleBorder(borderRadius: BorderRadius.circular(16));

  static TextStyle buttonTextStyle =
      TextStyle(fontSize: 14, fontFamily: fontFamily, height: 1);

  static ButtonStyle buttonStyle = TextButton.styleFrom(
    minimumSize: const Size(0, 40),
    shape: defaultButtonShape,
    padding: defaultButtonPadding,
    textStyle: buttonTextStyle,
    elevation: 0.0,
  );

  static ButtonStyle elevatedButtonStyle = ElevatedButton.styleFrom(
    minimumSize: const Size(0, 40),
    shape: defaultButtonShape,
    padding: defaultButtonPadding,
    textStyle: buttonTextStyle,
    backgroundColor: AppColors.blue,
    foregroundColor: AppColors.background,
    elevation: 0.0,
  );

  static String fontFamily = 'Lato';

  static TextTheme get textTheme => TextTheme(
        labelLarge: buttonTextStyle,
        labelSmall: TextStyle(
          fontSize: 14,
          fontFamily: fontFamily,
        ),
        bodyMedium: TextStyle(
          fontSize: 16,
          fontFamily: fontFamily,
        ),
        titleMedium: TextStyle(
          fontSize: 18,
          fontFamily: fontFamily,
        ),
        headlineMedium: TextStyle(
          fontSize: 20,
          fontFamily: fontFamily,
        ),
        displayMedium: TextStyle(
          fontSize: 22,
          fontFamily: fontFamily,
        ),
      );

  static ThemeData appTheme() => ThemeData.from(
        useMaterial3: true,
        textTheme: textTheme,
        colorScheme: ColorScheme.fromSwatch(
          brightness: Brightness.light,
        ).copyWith(
            background: AppColors.background,
            onBackground: AppColors.blue,
            secondary: AppColors.blue,
            surface: AppColors.background,
            onSurface: AppColors.blue),
      ).copyWith(
        floatingActionButtonTheme: const FloatingActionButtonThemeData(
            backgroundColor: AppColors.blue,
            foregroundColor: AppColors.background,
            elevation: 6,
            splashColor: Colors.blueAccent,
            shape: CircleBorder()),
        cardColor: AppColors.background,
        primaryColor: AppColors.yellow,
        appBarTheme: const AppBarTheme(
            color: AppColors.blue, foregroundColor: AppColors.background),
        disabledColor: AppColors.disabled,
        textButtonTheme: TextButtonThemeData(style: buttonStyle),
        elevatedButtonTheme: ElevatedButtonThemeData(
          style: elevatedButtonStyle,
        ),
        dividerColor: AppColors.disabled,
        tabBarTheme: const TabBarTheme(
          labelPadding: EdgeInsets.zero,
        ),
      );
}
