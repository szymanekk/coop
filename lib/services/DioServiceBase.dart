import 'clients/ClientBase.dart';

abstract class DioServiceBase {
  final ClientBase client;

  const DioServiceBase(this.client);
}