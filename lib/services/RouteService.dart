import 'package:coop/models/RouteEntity.dart';
import 'package:coop/resources/AppApis.dart';
import 'package:coop/services/DioServiceBase.dart';
import 'package:coop/services/routeServiceBase.dart';
import 'package:latlong2/latlong.dart';

class RouteService extends DioServiceBase implements RouteServiceBase {
  const RouteService(super.client);

  @override
  Future<RouteEntity> getRoute(
      LatLng currentLocation, LatLng shopLocation) async {
    final response = await client.fetchData(
        path:
            '${AppApis.mapRoute}/${currentLocation.longitude},${currentLocation.latitude};${shopLocation.longitude},${shopLocation.latitude}',
        parameters: Map.fromEntries([
          const MapEntry('alternatives', 'true'),
          const MapEntry('geometries', 'geojson'),
          const MapEntry('language', 'en'),
          const MapEntry('overview', 'full'),
          const MapEntry('steps', 'true'),
          MapEntry('access_token', AppApis.mapToken),
        ]));

    return List<RouteEntity>.from(
        response['routes'].map((model) => RouteEntity.fromDto(model))).first;
  }
}
