import 'package:coop/resources/AppApis.dart';
import 'package:coop/services/DioServiceBase.dart';
import 'package:coop/services/ShopServiceBase.dart';
import 'package:coop/models/ShopEntity.dart';

class ShopService extends DioServiceBase implements ShopServiceBase {
  const ShopService(super.client);

  @override
  Future<List<ShopEntity>> getShops(
      {String? postNumber, String? latitude, String? longitude}) async {
    final response = await client.fetchData(
        path: '${AppApis.stores}/SearchStores',
        parameters: Map.fromEntries([
          if (postNumber != null) MapEntry('searchInput', postNumber),
          if (latitude != null) MapEntry('locationLat', latitude),
          if (longitude != null) MapEntry('locationLon', longitude),
        ]));

    final objects = List<ShopEntity>.from(
        response['Stores'].map((model) => ShopEntity.fromDto(model)));
    return objects;
  }
}
