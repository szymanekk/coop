import 'package:coop/models/ShopEntity.dart';

abstract class ShopServiceBase {
  Future<List<ShopEntity>> getShops(
  {String? postNumber, String? latitude, String? longitude});
}
