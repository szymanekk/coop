import 'dart:async';

abstract class ClientBase {
  Future<void> init();

  Future<Map<String, dynamic>> fetchData(
      {required String path, Map<String, dynamic> parameters});
}
