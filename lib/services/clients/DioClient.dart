import 'dart:async';

import 'package:coop/models/Failure.dart';
import 'package:coop/resources/AppApis.dart';
import 'package:coop/resources/AppConstants.dart';
import 'package:dio/dio.dart';

import 'ClientBase.dart';

class DioClient implements ClientBase {
  late final Dio _dio;

  @override
  Future<void> init() async {
    _dio = Dio(
      BaseOptions(
          baseUrl: AppApis.baseUrl,
          connectTimeout: const Duration(seconds: 60),
          receiveTimeout: const Duration(seconds: 60),
          responseType: ResponseType.json
      ),
    );
  }

  @override
  Future<Map<String, dynamic>> fetchData(
      {required String path, Map<String, dynamic>? parameters}) async {
    final Response response = await _dio.get(
        path,
        queryParameters: parameters
    );
    if (response.statusCode == ConstantResponseCodes.success) {
      return response.data;
    }
    throw const Failure.unknown();
  }

}
