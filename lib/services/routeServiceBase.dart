import 'package:coop/models/RouteEntity.dart';
import 'package:latlong2/latlong.dart';

abstract class RouteServiceBase {
  Future<RouteEntity> getRoute(LatLng currentLocation, LatLng shopLocation);
}
