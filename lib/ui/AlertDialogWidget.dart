import 'package:flutter/material.dart';

class AlertDialogWidget extends StatelessWidget {
  final String title;
  final String bodyText;

  const AlertDialogWidget({super.key, required this.title, this.bodyText = ''});

  static Future<void> show(
      BuildContext context, {
        required String title,
        String bodyText = '',
      }) =>
      showDialog<bool?>(
        context: context,
        builder: (context) => AlertDialogWidget(
          title: title,
          bodyText: bodyText,
        ),
      );

  @override
  Widget build(BuildContext context) => AlertDialog(
    title: Text(title),
    content: Text(bodyText),
  );
}