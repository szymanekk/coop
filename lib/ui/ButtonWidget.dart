import 'package:flutter/material.dart';

enum ButtonTheme{light, dark}

class ButtonWidget extends StatelessWidget {
  final Function()? onPressed;
  final String label;
  final ButtonTheme theme;

  const ButtonWidget._(
      {this.onPressed,
        required this.theme,
      required this.label
      });

  factory ButtonWidget.dark({
    required String label,
    Function()? onPressed,
  }) =>
      ButtonWidget._(
        label: label,
        onPressed: onPressed,
        theme: ButtonTheme.dark,
      );

  factory ButtonWidget.light({
    required String label,
    Function()? onPressed,
  }) =>
      ButtonWidget._(
        label: label,
        onPressed: onPressed,
        theme: ButtonTheme.light,
      );

  Color getBgColor(BuildContext context) => switch(theme) {
    ButtonTheme.light => Theme.of(context).colorScheme.surface,
    ButtonTheme.dark => Theme.of(context).colorScheme.onBackground,
  };

  Color getFgColor(BuildContext context) => switch(theme) {
    ButtonTheme.light => Theme.of(context).colorScheme.secondary,
    ButtonTheme.dark => Theme.of(context).cardColor,
  };

  @override
  Widget build(BuildContext context) => ElevatedButton(
      style: Theme.of(context).elevatedButtonTheme.style?.copyWith(
          backgroundColor:
               MaterialStatePropertyAll<Color>(getBgColor(context)),
          foregroundColor:
               MaterialStatePropertyAll<Color>(getFgColor(context))),
      onPressed: onPressed,
      child: Text(label));
}


