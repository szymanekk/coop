import 'package:coop/resources/AppConstants.dart';
import 'package:flutter/material.dart';

class CircleButtonWidget extends StatelessWidget {
  final Function()? onPressed;
  final double? innerPadding;
  final IconData icon;
  final double? iconSize;
  final Color? bgColor;

  const CircleButtonWidget(
      {super.key,
      this.onPressed,
      this.innerPadding,
      required this.icon,
      this.iconSize,
      this.bgColor});

  @override
  Widget build(BuildContext context) => ElevatedButton(
      style: Theme.of(context).elevatedButtonTheme.style?.copyWith(
          backgroundColor: MaterialStatePropertyAll<Color>(
              bgColor ?? Theme.of(context).colorScheme.secondary),
          foregroundColor: MaterialStatePropertyAll<Color>(
              Theme.of(context).colorScheme.background),
          padding: MaterialStatePropertyAll<EdgeInsets>(
              EdgeInsets.all(innerPadding ?? UiConstant.defaultButtonPadding)),
          shape:
              const MaterialStatePropertyAll<OutlinedBorder>(CircleBorder())),
      onPressed: onPressed,
      child: Icon(
        icon,
        size: iconSize,
      ));
}
