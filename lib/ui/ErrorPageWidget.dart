import 'package:coop/resources/AppConstants.dart';
import 'package:coop/ui/ButtonWidget.dart';
import 'package:flutter/material.dart';
import 'package:coop/models/Failure.dart';
import 'package:coop/generated/l10n.dart';

class ErrorPageWidget extends StatelessWidget {
  const ErrorPageWidget(
      {super.key, required this.failure, this.onRefresh});

  final Failure failure;
  final Function()? onRefresh;
  final double _iconSize = 70.0;

  @override
  Widget build(BuildContext context) => Scaffold(
        body: Center(
          child: Card(
            child: Padding(
              padding: const EdgeInsets.all(UiConstant.cardPadding),
              child: Column(mainAxisSize: MainAxisSize.min, children: [
                Icon(
                  failure.getIcon(context),
                  size: _iconSize,
                  color: Theme.of(context).colorScheme.secondary,
                ),
                const SizedBox(
                  height: UiConstant.baseTextSpace * 2,
                ),
                Text(failure.getReadable(context)),
                const SizedBox(
                  height: UiConstant.baseTextSpace,
                ),
                if(failure.message != null)
                  Text(failure.message!),
                if(onRefresh != null)
                  SizedBox(
                    width: double.maxFinite,
                    child: ButtonWidget.light(
                        onPressed: onRefresh,
                        label: S.of(context).repeat),
                  )
              ]),
            ),
          ),
        ),
      );
}
