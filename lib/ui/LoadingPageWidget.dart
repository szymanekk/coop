import 'package:coop/resources/AppColors.dart';
import 'package:flutter/material.dart';

class LoadingWidget extends StatelessWidget {
  const LoadingWidget({super.key});

  final double _size = 30.0;

  @override
  Widget build(BuildContext context) => Scaffold(
    body: Center(
      child: SizedBox(
          width: _size,
          height: _size,
          child: const CircularProgressIndicator(color: AppColors.blue,)),
    ),
  );
}
