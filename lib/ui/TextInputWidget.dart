import 'package:coop/resources/AppColors.dart';
import 'package:coop/resources/AppConstants.dart';
import 'package:flutter/material.dart';

class TextInputWidget extends StatelessWidget {
  final Function(String)? onChange;
  final Widget? suffix;
  final String? hint;

  const TextInputWidget({super.key, this.onChange, this.suffix, this.hint});

  @override
  Widget build(BuildContext context) => TextField(
        autocorrect: false,
        onChanged: onChange,
        maxLines: 1,
        style: Theme.of(context).textTheme.bodyMedium,
        decoration: InputDecoration(
          filled: true,
          border: InputBorder.none,
          enabledBorder: OutlineInputBorder(
              borderSide: const BorderSide(
                color: AppColors.disabled,
                width: 1,
              ),
              borderRadius: BorderRadius.circular(
                UiConstant.defaultBorderRadius,
              )),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Theme.of(context).colorScheme.secondary,
              width: 1,
            ),
            borderRadius: BorderRadius.circular(
              UiConstant.defaultBorderRadius,
            ),
          ),
          contentPadding: const EdgeInsets.symmetric(
              horizontal: UiConstant.defaultButtonPadding),
          suffixIcon: suffix,
          hintText: hint ?? '',
          hintStyle: Theme.of(context).textTheme.labelSmall,
        ),
      );
}
