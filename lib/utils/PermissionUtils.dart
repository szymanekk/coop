import 'package:geolocator/geolocator.dart';

class PermissionUtils {
  static Future<bool> handlePermission() async {
    final geolocator = GeolocatorPlatform.instance;
    final serviceEnabled = await geolocator.isLocationServiceEnabled();
    LocationPermission permission = await geolocator.checkPermission();

    if (!serviceEnabled) {
      return false;
    }

    if (permission == LocationPermission.denied) {
      permission = await geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        return false;
      }
    }

    if (permission == LocationPermission.deniedForever) {
      return false;
    }

    return true;
  }
}
